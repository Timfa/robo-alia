var http = require('http');
var fs = require('fs');

var adventures = require('./adventures.js').adventures;
var zalgo = require('./zalgo.js').zalgo;
var allItems = require("./items.json");

if (!Object.entries)
{
    Object.entries = function (obj)
    {
        var ownProps = Object.keys(obj),
            i = ownProps.length,
            resArray = new Array(i); // preallocate the Array!
        while (i--)
            resArray[i] = [ownProps[i], obj[ownProps[i]]];

        return resArray;
    };
}

exports.admins =
    [
        "124136556578603009"
    ];

exports.cmds =
    [
        {//
            cmd: "help",
            params: "category",
            category: "main",
            execute: function (bot, info, args)
            {
                var cmds = ["Commands:"];
                var wip = ["Work In Progress (may not work correctly or at all):"];

                var categories = ["Categories:"];

                var category = args.join(" ").toLowerCase();

                if (args.length == 0)
                {
                    for (var i = 0; i < bot.commands.length; i++)
                    {
                        var found = false;

                        for (var c = 0; c < categories.length; c++)
                        {
                            if (categories[c] == "- " + bot.commands[i].category || bot.commands[i].hidden == true || bot.commands[i].category == "wip")
                            {
                                found = true;
                            }
                        }

                        if (!found)
                        {
                            categories.push("- " + bot.commands[i].category);
                        }
                    }

                    bot.sendMessage({
                        to: info.channelID,
                        message: "Help Categories: ```" + categories.join("\n") /*+ (wip.length > 1? "\n\n" + wip.join("\n") : "")*/ + "```",
                        typing: false
                    });
                }
                else
                {
                    for (var i = 0; i < bot.commands.length; i++)
                    {
                        if (!bot.commands[i].hidden && bot.commands[i].category == category)
                        {
                            var lines = "";

                            for (var a = 0; a < 15 - bot.commands[i].cmd.length; a++)
                                lines += "-";

                            if (!bot.commands[i].wip)
                                cmds.push(">" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
                            else
                                wip.push(">" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
                        }
                    }

                    bot.sendMessage({
                        to: info.channelID,
                        message: "Commands: ```" + cmds.join("\n") /*+ (wip.length > 1? "\n\n" + wip.join("\n") : "")*/ + "```",
                        typing: false
                    });
                }
            }
        },
        {
            cmd: "debughelp",
            params: "none",
            hidden: true,
            category: "admin",
            execute: function (bot, info, args)
            {
                var cmds = ["Commands:"];
                var hidden = ["Admin-only commands:"];

                for (var i = 0; i < bot.commands.length; i++)
                {
                    var lines = "";

                    for (var a = 0; a < 15 - bot.commands[i].cmd.length; a++)
                        lines += "-";

                    if (!bot.commands[i].hidden)
                        cmds.push(">" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
                    else
                        hidden.push(">" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "Admins: <@" + exports.admins.join("> <@") + "> ```" + /*cmds.join("\n") + "\n\n" + */ hidden.join("\n") + "```",
                    typing: false
                });
            }
        },
        {
            cmd: "voiceid",
            params: "none",
            hidden: false,
            category: "main",
            execute: function (bot, info, args)
            {
                var server = bot.servers[info.serverId];
                var voiceChannelID = server.members[info.userID].voice_channel_id;

                console.log("joining");
                bot.joinVoiceChannel(voiceChannelID, function (error, events)
                {

                    console.log("getting audio context");
                    bot.getAudioContext(voiceChannelID, function (error, stream) 
                    {
                        if (error) return console.error(error);

                        //Create a stream to your file and pipe it to the stream
                        //Without {end: false}, it would close up the stream, so make sure to include that.

                        console.log("streransd");
                        var str = fs.createReadStream('quack.wav');

                        stream.pipe(str, { end: false });
                        //str.pipe(stream, {end: false});

                        console.log("written");

                        console.log(stream);
                        //The stream fires `done` when it's got nothing else to send to Discord.
                        stream.on('done', function ()
                        {
                            console.log("SEEYA BYUEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
                            bot.leaveVoiceChannel(voiceChannelID, function (a) { console.log(a); });
                        });
                    });
                });


            }
        },
        {
            cmd: "recipe",
            params: "item name",
            category: "adventures",
            execute: function (bot, info, args)
            {
                var name = args.join(" ").trim();

                for (var i = 0; i < allItems.length; i++)
                {
                    if (allItems[i].name.toLowerCase() == name.toLowerCase() || allItems[i].plural.toLowerCase() == name.toLowerCase())
                    {
                        var newItem = JSON.parse(JSON.stringify(allItems[i]));

                        if (newItem.name == "Stock")
                        {
                            newItem.value = currentStockPrice(bot, info);
                        }

                        if (newItem.recipe !== null)
                        {
                            var itemnames = "";

                            for (var r = 0; r < newItem.recipe.length; r++)
                            {
                                var part = newItem.recipe[r];
                                var partName = "";

                                for (var pn = 0; pn < allItems.length; pn++)
                                {
                                    if (allItems[pn].name.toLowerCase() == part.name.toLowerCase() || allItems[pn].plural.toLowerCase() == part.name.toLowerCase())
                                    {
                                        partName = part.quantity !== 1 ? allItems[pn].plural : allItems[pn].name;
                                    }
                                }

                                itemnames += part.quantity + " " + partName + (r < newItem.recipe.length - 1 ? (part.consumed ? " (consumed)" : "") + ",\n" : (part.consumed ? " (consumed)" : ""));
                            }

                            bot.sendMessage({
                                to: info.channelID,
                                message: "Recipe for " + newItem.craftQuantity + " " + (newItem.craftQuantity > 1 ? newItem.plural : newItem.name) + ":\n```" + itemnames + "```",
                                typing: false
                            });
                        }
                        else
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "That item is not craftable.",
                                typing: true
                            });
                        }
                        return;
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "Item `" + name + "` doesn't exist.",
                    typing: false
                });
            }
        },
        {
            cmd: "craft",
            params: "item name",
            category: "adventures",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].inventory = bot.data[info.serverId].inventory || {};
                var items = bot.data[info.serverId].inventory[info.userID] || [];

                var name = args.join(" ").trim();

                for (var i = 0; i < allItems.length; i++)
                {
                    if (allItems[i].name.toLowerCase() == name.toLowerCase() || allItems[i].plural.toLowerCase() == name.toLowerCase())
                    {
                        var newItem = JSON.parse(JSON.stringify(allItems[i]));

                        if (newItem.name == "Stock")
                        {
                            newItem.value = currentStockPrice(bot, info);
                        }

                        if (newItem.recipe !== null)
                        {
                            var itemnames = "";
                            ////
                            var allFound = true;

                            for (var r = 0; r < newItem.recipe.length; r++)
                            {
                                var part = newItem.recipe[r];

                                var found = false;

                                for (var i = 0; i < items.length; i++)
                                {
                                    items[i] = fixItem(items[i]);

                                    if (items[i].name.toLowerCase() == part.name.toLowerCase() || (items[i].plural || "").toLowerCase() == part.name.toLowerCase())
                                    {
                                        quantity = part.quantity;

                                        if (items[i].quantity >= quantity)
                                        {
                                            found = true;
                                        }
                                        break;
                                    }
                                }

                                if (!found)
                                {
                                    allFound = false;
                                    break;
                                }
                            }

                            if (allFound)
                            {
                                for (var r = 0; r < newItem.recipe.length; r++)
                                {
                                    var part = newItem.recipe[r];

                                    if (part.consumed)
                                    {
                                        for (var i = 0; i < items.length; i++)
                                        {
                                            if (items[i].name.toLowerCase() == part.name.toLowerCase() || (items[i].plural || "").toLowerCase() == part.name.toLowerCase())
                                            {
                                                quantity = part.quantity;

                                                if (items[i].quantity >= quantity)
                                                {
                                                    items[i].quantity -= quantity;

                                                    items = removeEmptyFromInventory(items);

                                                    bot.data[info.serverId].inventory[info.userID] = items;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }

                                addToUserInventory(bot, info.userID, newItem, info.serverId, newItem.craftQuantity);

                                bot.sendMessage({
                                    to: info.channelID,
                                    message: "Crafted " + newItem.craftQuantity + " " + (newItem.craftQuantity > 1 ? newItem.plural : newItem.name) + "!",
                                    typing: false
                                });
                            }
                            else
                            {
                                bot.sendMessage({
                                    to: info.channelID,
                                    message: "You don't have all the requirements to craft this!",
                                    typing: false
                                });
                            }

                        }
                        else
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "That item is not craftable.",
                                typing: true
                            });
                        }
                        return;
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "Item `" + name + "` doesn't exist.",
                    typing: false
                });
            }
        },
        {
            cmd: "serverid",
            params: "none",
            hidden: true,
            category: "admin",
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "Server ID: `" + bot.data[info.serverId].serverId + "`",
                    typing: false
                });
            }
        },
        {
            cmd: "cet",
            params: "none",
            category: "queries",
            execute: function (bot, info, args)
            {
                var d = new Date();
                bot.sendMessage({
                    to: info.channelID,
                    message: "Current Central European Time: `" + (d.getUTCHours() + 1) + ":" + (d.getUTCMinutes()) + "`.",
                    typing: false
                });
            }
        },
        {
            cmd: "invitelink",
            params: "none",
            category: "main",
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "https://discordapp.com/oauth2/authorize?&client_id=479575060345257986&scope=bot&permissions=511040",
                    typing: false
                });
            }
        },
        {
            cmd: "ping",
            params: "none",
            category: "main",
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "<@!" + info.userID + ">" + ' Pong!',
                    typing: false
                });
            }
        },
        {
            cmd: "update",
            params: "none",
            hidden: true,
            category: "admin",
            execute: function (bot, info, args)
            {
                var exec = require('child_process').exec;

                bot.data.update = info.channelID;

                bot.sendMessage({
                    to: info.channelID,
                    message: "Fetching changes...",
                    typing: false
                }, function ()
                {
                    exec('git fetch --all', function (err, stdout, stderr) 
                    {
                        exec('git log --oneline master..origin/master', function (err, stdout, stderr) 
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: (stdout == "" ? "No changes, " : "Changes: ```" + stdout + "``` Updating and ") + "reloading!",
                                typing: false
                            }, function ()
                            {
                                bot.suicide();
                            });
                        });
                    });
                });
            }
        },
        {
            cmd: "roll",
            params: "dice string (1d20+5)",
            category: "fun",
            execute: function (bot, info, args)
            {
                var diceStr = args.join(" ");

                bot.sendMessage({
                    to: info.channelID,
                    message: parseTextDice(diceStr),
                    typing: true
                });
            }
        },
        {
            cmd: "pat",
            params: "@mention (optional, omit to pat me!)",
            category: "fun",
            execute: function (bot, info, args)
            {
                if (args.length == 0)
                {
                    bot.addReaction({
                        channelID: info.channelID,
                        messageID: info.evt.d.id,
                        reaction: "❤"
                    });

                    bot.sendMessage({
                        to: info.channelID,
                        message: ":3",
                        typing: true
                    });

                    bot.data[info.serverId].pats = bot.data[info.serverId].pats || {};

                    bot.data[info.serverId].pats[info.userID] = (bot.data[info.serverId].pats[info.userID] || 0) - -1;
                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "_pats " + args[0] + "_",
                        typing: true
                    });
                }
            }
        },
        {
            cmd: "favorite",
            params: "none",
            category: "fun",
            execute: function (bot, info, args)
            {
                patters = bot.data[info.serverId].pats || {};

                var fav = 0;
                var pats = 0;

                console.log(patters);

                for (var key in patters)
                {
                    if (patters[key] > pats)
                    {
                        fav = key;
                        pats = patters[key];
                    }
                };

                if (fav !== 0)
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "❤<@!" + fav + ">❤",
                        typing: true
                    });
                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "idk",
                        typing: true
                    });
                }
            }
        },
        {
            cmd: "echo",
            params: "anything",
            category: "admin",
            hidden: true,
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: args.join(" "),
                    typing: true
                });
            }
        },
        {
            cmd: "rawecho",
            params: "anything",
            category: "admin",
            hidden: true,
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "```" + args.join(" ") + "```",
                    typing: true
                });
            }
        },
        {
            cmd: "zalgo",
            params: "a̴͚̟n̠͆͡y͏̛͞t̏́͆h̑͋͊i̻͟͝n͍͘͠g̉́̽",
            category: "fun",
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: zalgo.generate(args.join(" ")),
                    typing: true
                });
            }
        },
        {
            cmd: "erasmus",
            params: "Anything",
            category: "fun",
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "```" + ErasmusText(args.join(" ")) + "```",
                    typing: false
                });
            }
        },
        {
            cmd: "memdump",
            params: "none",
            hidden: true,
            category: "admin",
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "```" + JSON.stringify(bot.data[info.serverId]) + "```",
                    typing: false
                });
            }
        },
        {
            cmd: "globalmemdump",
            params: "none",
            hidden: true,
            category: "admin",
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "```" + JSON.stringify(bot.data) + "```",
                    typing: false
                });
            }
        },
        {
            cmd: "clearlocalmem",
            params: "none",
            hidden: true,
            category: "admin",
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "Clearing memory!",
                    typing: false
                });

                bot.data[info.serverId] = {};
            }
        },
        {
            cmd: "clearglobalmem",
            params: "none (untested)",
            hidden: true,
            category: "admin",
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "Clearing global memory!",
                    typing: false
                });

                bot.data = {};
            }
        },
        {
            cmd: "encode",
            params: "anything",
            category: "fun",
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "Encoded! ```" + Buffer.from(args.join(" ")).toString('base64') + "```",
                    typing: false
                });
            }
        },
        {
            cmd: "decode",
            params: "anything",
            category: "fun",
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "Decoded! ```" + Buffer.from(args[0], 'base64').toString() + "```",
                    typing: false
                });
            }
        },
        {
            cmd: "play",
            params: "'playing' string",
            category: "meta",
            execute: function (bot, info, args)
            {
                bot.setPresence({
                    game:
                    {
                        name: args.join(" "),
                        type: 0
                    }
                });

                bot.sendMessage({
                    to: info.channelID,
                    message: "_Now playing " + args.join(" ") + "_",
                    typing: false
                });
            }
        },
        {
            cmd: "stream",
            params: "'streaming' string",
            category: "meta",
            execute: function (bot, info, args)
            {
                bot.setPresence({
                    game:
                    {
                        name: args.join(" "),
                        type: 1
                    }
                });

                bot.sendMessage({
                    to: info.channelID,
                    message: "_Now streaming " + args.join(" ") + "_",
                    typing: false
                });
            }
        },
        {
            cmd: "listento",
            params: "'listening to' string",
            category: "meta",
            execute: function (bot, info, args)
            {
                bot.setPresence({
                    game:
                    {
                        name: args.join(" "),
                        type: 2
                    }
                });

                bot.sendMessage({
                    to: info.channelID,
                    message: "_Now listening to " + args.join(" ") + "_",
                    typing: false
                });
            }
        },
        {
            cmd: "watch",
            params: "'watching' string",
            category: "meta",
            execute: function (bot, info, args)
            {
                bot.setPresence({
                    game:
                    {
                        name: args.join(" "),
                        type: 3
                    }
                });

                bot.sendMessage({
                    to: info.channelID,
                    message: "_Now watching " + args.join(" ") + "_",
                    typing: false
                });
            }
        },
        {
            cmd: "google",
            params: "search query",
            category: "queries",
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "http://lmgtfy.com/?q=" + args.join("+"),
                    typing: true
                });
            }
        },
        {
            cmd: "inventory",
            params: "none",
            category: "adventures",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].inventory = bot.data[info.serverId].inventory || {};
                var items = bot.data[info.serverId].inventory[info.userID] || [];

                var mention = info.userID;

                if (args.length > 0)
                {
                    var validMention = args[0].match(/(<@)!?([0-9]+)(>)/);

                    if (validMention)
                    {
                        bot.data[info.serverId].inventory = bot.data[info.serverId].inventory || {};

                        mention = validMention[2];
                        items = bot.data[info.serverId].inventory[mention] || [];
                    }
                }

                var lines = [];
                var multi = false;
                var multiI = 0;

                var totalValue = 0;

                items.sort(function (a, b)
                {
                    return (b.quantity * b.value) - (a.quantity * a.value);
                });

                for (var i = 0; i < items.length; i++)
                {
                    var dashes = "";
                    var spaces = "";

                    items[i] = fixItem(items[i]);

                    var itemDat = findItemByName(items[i].name);
                    var itemName = (items[i].quantity == 1 ? itemDat.name : itemDat.plural);

                    for (var a = 0; a < 25 - (itemName.length + items[i].quantity.toString().length); a++)
                        dashes += "-";

                    if (items[i].name == "Stock")
                        items[i].value = currentStockPrice(bot, info);

                    for (var a = 0; a < 6 - (items[i].value).toString().length; a++)
                        spaces += " ";

                    if (multi && lines[multiI] == null)
                    {
                        lines[multiI] = [];
                    }

                    if (!multi)
                        lines.push(items[i].quantity + " " + itemName + " " + dashes + " value: " + (items[i].value) + spaces + "(" + (items[i].value * items[i].quantity) + " total)");
                    else
                        lines[multiI].push(items[i].quantity + " " + itemName + " " + dashes + " value: " + (items[i].value) + spaces + "(" + (items[i].value * items[i].quantity) + " total)");

                    totalValue += (items[i].value * items[i].quantity);

                    if (lines.length >= 20 && !multi)
                    {
                        lines = [lines];
                        multi = true;
                        multiI = 1;
                    }
                    else if (multi && lines[multiI].length >= 20)
                    {
                        multiI++;
                    }
                }

                if (lines.length == 0)
                    lines.push("Nothing!");

                if (!multi)
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "<@" + mention + ">'s Total Inventory Value: " + totalValue + " ```" + lines.join("\n") + "```",
                        typing: false
                    });
                }
                else
                {
                    var p2 = function (id, arr)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "<@" + mention + ">'s Inventory (" + (id - -1) + "/" + lines.length + "): ```" + lines[id].join("\n") + "```",
                            typing: false
                        }, function ()
                        {
                            if (arr.length > id + 1)
                                p2(id + 1, arr);
                        });
                    };

                    bot.sendMessage({
                        to: info.channelID,
                        message: "<@" + mention + ">'s Total Inventory Value : " + totalValue + " (1/" + lines.length + ") ```" + lines[0].join("\n") + "```",
                        typing: false
                    }, function ()
                    {
                        p2(1, lines);
                    });
                }
            }
        },
        {
            cmd: "summon",
            params: "quantity(optional), item name",
            hidden: true,
            category: "adventures",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].inventory = bot.data[info.serverId].inventory || {};
                var items = bot.data[info.serverId].inventory[info.userID] || [];

                var quantity = args[0].match(/[0-9]+/);

                if (quantity)
                {
                    args.splice(0, 1);
                }
                else
                {
                    quantity = 1;
                }

                var name = args.join(" ").trim();

                for (var i = 0; i < allItems.length; i++)
                {
                    if (allItems[i].name.toLowerCase() == name.toLowerCase() || allItems[i].plural.toLowerCase() == name.toLowerCase())
                    {
                        var newItem = JSON.parse(JSON.stringify(allItems[i]));

                        addToUserInventory(bot, info.userID, newItem, info.serverId, quantity);

                        var item = findItemByName(name);

                        var itemName = (quantity == 1 ? item.name : item.plural);

                        bot.sendMessage({
                            to: info.channelID,
                            message: "You summon " + quantity + " " + itemName,
                            typing: false
                        });

                        return;
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "Item `" + name + "` doesn't exist.",
                    typing: false
                });
            }
        },
        {
            cmd: "shop",
            params: "none",
            category: "adventures",
            execute: function (bot, info, args)
            {
                var priceList = [];
                var text = [];

                for (var i = 0; i < allItems.length; i++)
                {
                    if (allItems[i].buyable)
                    {
                        if (allItems[i].name == "Stock")
                        {
                            allItems[i].value = currentStockPrice(bot, info);
                        }

                        priceList.push(allItems[i]);
                    }
                }

                for (var i = 0; i < priceList.length; i++)
                {
                    var lines = "";

                    for (var a = 0; a < 25 - priceList[i].name.length; a++)
                        lines += "-";

                    text.push(priceList[i].name + " " + lines + " Price: " + priceList[i].value);
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "```" + text.join("\n") + "```",
                    typing: false
                });
            }
        },
        {
            cmd: "price",
            params: "item name",
            category: "adventures",
            execute: function (bot, info, args)
            {
                var name = args.join(" ").trim();

                for (var i = 0; i < allItems.length; i++)
                {
                    if (allItems[i].name.toLowerCase() == name.toLowerCase() || allItems[i].plural.toLowerCase() == name.toLowerCase())
                    {
                        var newItem = JSON.parse(JSON.stringify(allItems[i]));

                        if (newItem.name == "Stock")
                        {
                            newItem.value = currentStockPrice(bot, info);
                        }

                        if (newItem.buyable)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: (newItem.name == "Stock" ? "Current stock price:" : "") + " ```" + newItem.name + ": " + newItem.value + " coins.```",
                                typing: true
                            });
                        }
                        else
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "That item is not for sale.",
                                typing: true
                            });
                        }
                        return;
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "Item `" + name + "` doesn't exist.",
                    typing: false
                });
            }
        },
        {
            cmd: "buy",
            params: "quantity(optional), item name",
            category: "adventures",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].inventory = bot.data[info.serverId].inventory || {};
                var items = bot.data[info.serverId].inventory[info.userID] || [];

                var quantity = args[0].match(/[0-9]+|all|All|ALL/);

                if (quantity)
                {
                    args.splice(0, 1);
                }
                else
                {
                    quantity = 1;
                }

                var name = args.join(" ").trim();

                for (var i = 0; i < allItems.length; i++)
                {
                    if (allItems[i].name.toLowerCase() == name.toLowerCase() || allItems[i].plural.toLowerCase() == name.toLowerCase())
                    {
                        var newItem = JSON.parse(JSON.stringify(allItems[i]));

                        var itemName = (quantity == 1 ? newItem.name : newItem.plural);

                        if (newItem.name == "Stock")
                        {
                            newItem.value = currentStockPrice(bot, info);
                        }

                        var canBuy = false;
                        var totalPrice = 0;

                        if (newItem.buyable)
                        {
                            if (quantity.toString().toLowerCase() == "all")
                            {
                                for (var s = 0; s < items.length; s++)
                                {
                                    if (items[s].name.toLowerCase() == "coin")
                                    {
                                        quantity = Math.floor(items[s].quantity / newItem.value);

                                        if (quantity == 0)
                                            quantity = 1;

                                        totalPrice = newItem.value * quantity;

                                        if (items[s].quantity >= totalPrice)
                                        {
                                            canBuy = true;
                                            items[s].quantity -= totalPrice;
                                            items = removeEmptyFromInventory(items);
                                            bot.data[info.serverId].inventory[info.userID] = items;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                totalPrice = quantity * newItem.value;

                                for (var s = 0; s < items.length; s++)
                                {
                                    if (items[s].name.toLowerCase() == "coin")
                                    {
                                        if (items[s].quantity >= totalPrice)
                                        {
                                            canBuy = true;
                                            items[s].quantity -= totalPrice;

                                            items = removeEmptyFromInventory(items);

                                            bot.data[info.serverId].inventory[info.userID] = items;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (canBuy)
                            {
                                addToUserInventory(bot, info.userID, newItem, info.serverId, quantity);

                                bot.sendMessage({
                                    to: info.channelID,
                                    message: "You buy " + (quantity == 1 ? newItem.an.toLowerCase() : quantity) + " " + itemName + " for " + totalPrice + " coins.",
                                    typing: false
                                });
                            }
                            else
                            {
                                bot.sendMessage({
                                    to: info.channelID,
                                    message: "You don't have enough money.",
                                    typing: true
                                });
                            }

                            return;
                        }
                        else
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "That item is not for sale.",
                                typing: true
                            });
                            return;
                        }
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "Item `" + name + "` doesn't exist.",
                    typing: true
                });
            }
        },
        {
            cmd: "sell",
            params: "quantity(optional), item name",
            category: "adventures",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].inventory = bot.data[info.serverId].inventory || {};
                var items = bot.data[info.serverId].inventory[info.userID] || [];

                var quantity = args[0].match(/[0-9]+|all|All|ALL/);

                if (quantity)
                {
                    args.splice(0, 1);
                }
                else
                {
                    quantity = 1;
                }

                var name = args.join(" ").trim();

                if (name.toLowerCase() == "coin")
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "That's just silly.",
                        typing: true
                    });
                    return;
                }

                var found = false;
                var properName = name;

                var tvalue = 0;

                for (var i = 0; i < items.length; i++)
                {
                    items[i] = fixItem(items[i]);

                    if (items[i].name.toLowerCase() == name.toLowerCase() || (items[i].plural || "").toLowerCase() == name.toLowerCase())
                    {
                        if (quantity.toString().toLowerCase() == "all")
                        {
                            quantity = items[i].quantity;
                        }

                        if (items[i].quantity >= quantity)
                        {
                            if (items[i].name == "Stock")
                            {
                                items[i].value = currentStockPrice(bot, info);
                            }

                            found = true;
                            properName = items[i].name;
                            items[i].quantity -= quantity;

                            tvalue = Math.ceil(items[i].value * (items[i].name == "Stock" ? 1 : 0.9)) * quantity;

                            items = removeEmptyFromInventory(items);

                            addToUserInventory(bot, info.userID, { name: "Coin", value: 1 }, info.serverId, tvalue);
                        }
                        break;
                    }
                }

                var item = findItemByName(properName);

                var itemName = (quantity == 1 ? item.name : item.plural);

                if (found)
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "Sold " + (quantity == 1 ? item.an.toLowerCase() : quantity) + " " + itemName + " for " + tvalue + " coins!",
                        typing: false
                    });
                }
                else
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "You don't have " + (quantity == 1 ? item.an.toLowerCase() : quantity) + " " + itemName + ".",
                        typing: false
                    });
                }
            }
        },
        {
            cmd: "give",
            params: "@mention target, quantity(optional), item name",
            category: "adventures",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].inventory = bot.data[info.serverId].inventory || {};
                var items = bot.data[info.serverId].inventory[info.userID] || [];

                var validMention = args[0].match(/(<@)!?([0-9]+)(>)/);
                if (!validMention)
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "Invalid target.",
                        typing: false
                    });
                    return;
                }

                var mention = validMention[2];

                if (validMention)
                {
                    var targetInv = bot.data[info.serverId].inventory[mention] || [];

                    args = args.slice(1);

                    var quantity = args[0].match(/[0-9]+|all|All|ALL/);

                    if (quantity)
                    {
                        args.splice(0, 1);
                    }
                    else
                    {
                        quantity = 1;
                    }

                    if (quantity <= 0)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "You give <@" + mention + "> nothing.",
                            typing: false
                        });
                    }
                    else
                    {
                        var name = args.join(" ").trim();

                        var found = false;
                        var properName = name;

                        for (var i = 0; i < items.length; i++)
                        {
                            items[i] = fixItem(items[i]);

                            if (items[i].name.toLowerCase() == name.toLowerCase() || (items[i].plural || "").toLowerCase() == name.toLowerCase())
                            {
                                if (quantity.toString().toLowerCase() == "all")
                                {
                                    quantity = items[i].quantity;
                                }

                                if (items[i].quantity >= quantity)
                                {
                                    found = true;
                                    properName = items[i].name;
                                    items[i].quantity -= quantity;

                                    var toGive = JSON.parse(JSON.stringify(items[i]));
                                    toGive.quantity = quantity;

                                    items = removeEmptyFromInventory(items);

                                    bot.data[info.serverId].inventory[info.userID] = items;

                                    addToUserInventory(bot, mention, toGive, info.serverId, quantity);
                                }
                                break;
                            }
                        }

                        var item = findItemByName(properName);

                        var itemName = (quantity == 1 ? item.name : item.plural);

                        if (found)
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "giving " + (quantity == 1 ? item.an.toLowerCase() : quantity) + " " + itemName + " to <@" + mention + ">",
                                typing: false
                            });
                        }
                        else
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "You don't have " + (quantity == 1 ? item.an.toLowerCase() : quantity) + " " + itemName + ".",
                                typing: false
                            });
                        }
                    }
                }
            }
        },
        {
            cmd: "discard",
            params: "quantity(optional), item name",
            category: "adventures",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].inventory = bot.data[info.serverId].inventory || {};
                var items = bot.data[info.serverId].inventory[info.userID] || [];

                var quantity = args[0].match(/[0-9]+|all|All|ALL/);

                if (quantity)
                {
                    args.splice(0, 1);
                }
                else
                {
                    quantity = 1;
                }

                if (quantity <= 0)
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "You discard nothing.",
                        typing: false
                    });
                }
                else
                {
                    var name = args.join(" ").trim();

                    var found = false;
                    var properName = name;

                    for (var i = 0; i < items.length; i++)
                    {
                        items[i] = fixItem(items[i]);

                        if (items[i].name.toLowerCase() == name.toLowerCase() || (items[i].plural || "").toLowerCase() == name.toLowerCase())
                        {
                            if (quantity.toString().toLowerCase() == "all")
                            {
                                quantity = items[i].quantity;
                            }

                            if (items[i].quantity >= quantity)
                            {
                                found = true;
                                properName = items[i].name;
                                items[i].quantity -= quantity;

                                items = removeEmptyFromInventory(items);

                                bot.data[info.serverId].inventory[info.userID] = items;
                            }
                            break;
                        }
                    }

                    var item = findItemByName(properName);

                    var itemName = (quantity == 1 ? item.name : item.plural);

                    if (found)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "Discarding " + (quantity == 1 ? item.an.toLowerCase() : quantity) + " " + itemName + ".",
                            typing: false
                        });
                    }
                    else
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "You don't have " + (quantity == 1 ? item.an.toLowerCase() : quantity) + " " + itemName + ".",
                            typing: false
                        });
                    }
                }
            }
        },
        {
            cmd: "resetadventurelock",
            params: "None",
            category: "admin",
            hidden: true,
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].adventurelock = [];

                bot.sendMessage({
                    to: info.channelID,
                    message: "Adventure lock reset.",
                    typing: false
                });
            }
        },
        {
            cmd: "adventure",
            params: "@mention companions (optional, any number)",
            category: "adventures",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].adventurelock = bot.data[info.serverId].adventurelock || [];

                for (var i = 0; i < bot.data[info.serverId].adventurelock.length; i++)
                {
                    if (bot.data[info.serverId].adventurelock[i] == info.userID)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "You're already in an adventure!",
                            typing: false
                        });
                        return;
                    }
                }

                var noJoin = [];

                var group = [info.userID];
                bot.data[info.serverId].adventurelock.push(info.userID);

                var asMention = ["<@" + info.userID + ">"];

                bot.data[info.serverId].inventory = bot.data[info.serverId].inventory || {};

                for (var i = 0; i < args.length; i++)
                {
                    var allMentions = args[i].match(/(<@)!?([0-9]+)(>)/g);

                    if (allMentions && allMentions.length > 0)
                    {
                        for (var m = 0; m < allMentions.length; m++)
                        {
                            var validMention = allMentions[m].match(/(<@)!?([0-9]+)(>)/);
                            if (validMention)
                            {
                                if (bot.data[info.serverId].adventurelock.indexOf(validMention[2]) < 0)
                                {
                                    if (group.indexOf(validMention[2]) < 0)
                                    {
                                        group.push(validMention[2]);
                                        asMention.push("<@" + validMention[2] + ">");
                                        bot.data[info.serverId].adventurelock.push(validMention[2]);
                                    }
                                }
                                else
                                {
                                    if (noJoin.indexOf("<@" + validMention[2] + ">") < 0)
                                    {
                                        noJoin.push("<@" + validMention[2] + ">");
                                    }
                                }
                            }
                        }
                    }
                }

                var found = false;

                var sI = Math.round(Math.random() * (adventures.length - 1));
                var areaName = "";

                while (!found)
                {
                    if (adventures[sI].canStart)
                    {
                        found = true;
                        areaName = adventures[sI].name;
                    }
                    else
                    {
                        sI++;

                        if (sI > adventures.length - 1)
                            sI = 0;
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "You enter the " + areaName + "...\nYour party:\n" + asMention.join("\n") + (noJoin.length > 0 ? "\nThese adventurers are already in an adventure and can't join:\n" + noJoin.join("\n") : ""),
                    typing: false
                }, function ()
                {
                    var id = info.evt.d.guild_id || info.userID;

                    adventureStep(bot, info, group, asMention, id, areaName, 0, 0);
                });
            }
        },
        {
            cmd: "adventuredebug",
            params: "none",
            category: "admin",
            hidden: true,
            execute: function (bot, info, args)
            {
                bot.data.lastAdventure = bot.data.lastAdventure || {};

                var str =
                    "Area: " + bot.data.lastAdventure.areaName
                    + " (ID " + bot.data.lastAdventure.index
                    + ")\nStep " + bot.data.lastAdventure.stepIndex
                    + ":\n " + bot.data.lastAdventure.step;

                bot.sendMessage({
                    to: info.channelID,
                    message: str,
                    typing: false
                });
            }
        },
        {
            cmd: "teamadventure",
            params: "None",
            category: "teams",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].adventurelock = bot.data[info.serverId].adventurelock || [];

                for (var i = 0; i < bot.data[info.serverId].adventurelock.length; i++)
                {
                    if (bot.data[info.serverId].adventurelock[i] == info.userID)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "You're already in an adventure!",
                            typing: false
                        });
                        return;
                    }
                }

                var noJoin = [];

                var group = [info.userID];
                bot.data[info.serverId].adventurelock.push(info.userID);

                var asMention = ["<@" + info.userID + ">"];

                bot.data[info.serverId].inventory = bot.data[info.serverId].inventory || {};

                var teamName = "";

                bot.data[info.serverId].teams = bot.data[info.serverId].teams || [];
                var teams = bot.data[info.serverId].teams;
                var memberList = [];

                var teamFound = false;

                for (var i = 0; i < teams.length; i++)
                {
                    if (teamFound)
                        break;

                    memberList = [];

                    for (var m = 0; m < teams[i].members.length; m++)
                    {
                        if (teams[i].members[m] == info.userID)
                        {
                            teamName = teams[i].name;
                            teamFound = true;
                        }
                        else
                        {
                            memberList.push(teams[i].members[m]);
                        }
                    }
                }

                if (teamName.length == 0)
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "You aren't part of a team!",
                        typing: false
                    });
                    return;
                }

                for (var i = 0; i < memberList.length; i++)
                {
                    if (bot.data[info.serverId].adventurelock.indexOf(memberList[i]) < 0)
                    {
                        group.push(memberList[i]);
                        asMention.push("<@" + memberList[i] + ">");
                        bot.data[info.serverId].adventurelock.push(memberList[i]);
                    }
                    else
                    {
                        if (noJoin.indexOf("<@" + memberList[i] + ">") < 0)
                        {
                            noJoin.push("<@" + memberList[i] + ">");
                        }
                    }
                }

                var found = false;

                var sI = Math.round(Math.random() * (adventures.length - 1));
                var areaName = "";

                while (!found)
                {
                    if (adventures[sI].canStart)
                    {
                        found = true;
                        areaName = adventures[sI].name;
                    }
                    else
                    {
                        sI++;

                        if (sI > adventures.length - 1)
                            sI = 0;
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: teamName + " enters the " + areaName + "..." + (noJoin.length > 0 ? "\n\nThese adventurers are already in an adventure and can't join:\n" + noJoin.join("\n") : ""),
                    typing: false
                }, function ()
                {
                    var id = info.evt.d.guild_id || info.userID;

                    adventureStep(bot, info, group, asMention, id, areaName, 0, 0);
                });
            }
        },
        {
            cmd: "forcebackup",
            params: "nothing",
            category: "admin",
            hidden: true,
            execute: function (bot, info, args)
            {
                fs.writeFile("./botDataBackup.json", JSON.stringify(bot.data), function (err)
                {
                    if (err)
                    {
                        console.log(err);
                        bot.sendMessage({
                            to: info.channelID,
                            message: "Failed to create a back-up.",
                            typing: false
                        });
                    }
                    else
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "Bot data back-up created.",
                            typing: false
                        });
                    }
                });
            }
        },
        {
            cmd: "rankings",
            params: "none",
            category: "adventures",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].inventory = bot.data[info.serverId].inventory || {};

                var invs = Object.entries(JSON.parse(JSON.stringify(bot.data[info.serverId].inventory)));

                var ranks = [];

                for (var i = 0; i < invs.length; i++)
                {
                    var totalValue = 0;

                    var items = invs[i][1];

                    totalValue += getInventoryTotalValue(invs[i][1], bot, info);

                    ranks.push({ id: invs[i][0], val: totalValue });
                }

                ranks.sort(function (a, b) { return b.val - a.val; });

                if (ranks.length > 10)
                    ranks.splice(10, ranks.length - 10);

                var str = "Top 10 Wealthiest Adventurers: \n";

                for (var i = 0; i < ranks.length; i++)
                {
                    str += (i + 1) + ": " + (i < 9 ? " " : "") + "<@!" + ranks[i].id + "> at an inventory value of " + ranks[i].val + (ranks[i].val == 1 ? " coins!\n" : " coins!\n");
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: str,
                    typing: false
                });
            }
        },
        {
            cmd: "sadness",
            params: "nothing",
            category: "fun",
            execute: function (bot, info, args)
            {
                var url = "sad.mp3";

                bot.uploadFile({
                    to: info.channelID,
                    file: url
                }, function (error, response)
                {
                    console.log(error);
                });
            }
        },
        {
            cmd: "givefile",
            params: "nothing",
            category: "admin",
            hidden: true,
            execute: function (bot, info, args)
            {
                var url = args.join(" ");

                bot.uploadFile({
                    to: info.channelID,
                    file: url
                }, function (error, response)
                {
                    console.log(error);
                });
            }
        },
        {
            cmd: "translate",
            params: "target language code (OPTIONAL), source text",
            category: "queries",
            execute: function (bot, info, args)
            {
                var target = "en";
                var triedTarget = false;

                if (args[0][0] == "[" && args[0][args[0].length - 1] == "]")
                {
                    triedTarget = true;
                    target = args[0];
                    args.splice(0, 1);

                    target = target.replace("[", "");
                    target = target.replace("]", "");
                }

                var str = args.join(" ");
                var url = "https://translation.googleapis.com/language/translate/v2?q=" + encodeURIComponent(str) + "&target=" + target + "&format=text&key=AIzaSyBKq9FKCmqm43qcGjlkjXZmKCir4dxk-54";
                var https = require("https");

                https.get(url, function (response)
                {
                    var data = '';

                    response.on('data', function (chunk)
                    {
                        data += chunk;
                    });

                    response.on('end', function () 
                    {
                        var obj = JSON.parse(data);

                        if (!obj.data || !obj.data.translations)
                        {
                            if (obj.error && obj.error.code == 403)
                            {
                                bot.sendMessage({
                                    to: info.channelID,
                                    message: "I can't translate anything for a while, sorry!",
                                    typing: false
                                });
                            }
                            else
                            {
                                bot.sendMessage({
                                    to: info.channelID,
                                    message: "The target language code was invalid. Try using [en], [de], [nl] or similar codes.",
                                    typing: false
                                });
                            }
                        }
                        else
                        {
                            /*bot.editMessage({
                               channelID: info.channelID,
                               messageID: info.evt.d.id,
                               message: "<:atl:704690621939581021>" + obj.data.translations[0].translatedText + "<:atr:704690622216405088>"
                            })*/
                            bot.sendMessage({
                                to: info.channelID,
                                message: "<@!" + info.userID + ">: <:atl:704690621939581021>" + obj.data.translations[0].translatedText + "<:atr:704690622216405088>",
                                typing: str.length < 50
                            });
                        }
                    });
                });
            }
        },
        {
            cmd: "ask",
            params: "whatever you want to know!",
            category: "queries",
            execute: function (bot, info, args)
            {
                var https = require("https");

                var url = "https://api.wolframalpha.com/v1/result?i=" + args.join("+") + "&appid=2H3Q9V-AAGLP6K24U";

                https.get(url, function (response)
                {
                    var data = '';

                    response.on('data', function (chunk)
                    {
                        data += chunk;
                    });

                    response.on('end', function () 
                    {
                        if (data == "Wolfram|Alpha did not understand your input")
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "I don't understand your question.",
                                typing: true
                            });
                        }
                        else
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: data,
                                typing: true
                            });
                        }

                    });
                });
            }
        },
        {
            cmd: "topmentions",
            params: "none",
            category: "main",
            execute: function (bot, info, args)
            {
                var mentions = Object.entries(bot.data[info.serverId].mentions);

                mentions.sort(function (a, b) { return b[1] - a[1]; });

                if (mentions.length > 10)
                    mentions.splice(10, mentions.length - 10);

                var str = "Top 10 Mentioned Users: \n";

                for (var i = 0; i < mentions.length; i++)
                {
                    str += (i + 1) + ": " + (i < 9 ? " " : "") + "<@!" + mentions[i][0] + "> at " + mentions[i][1] + (mentions[i][1] == 1 ? " mention!\n" : " mentions!\n");
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: str,
                    typing: false
                });
            }
        },
        {
            cmd: "adventureareas",
            params: "none",
            category: "adventures",
            execute: function (bot, info, args)
            {
                var str = "";

                var highest = 0;

                for (var i = 0; i < adventures.length; i++)
                {
                    if (adventures[i].events.length > highest)
                        highest = adventures[i].events.length;
                }

                for (var i = 0; i < adventures.length; i++)
                {
                    str += "Area: " + adventures[i].name + "\ncompleteness: " + Math.round((adventures[i].events.length / highest) * 100) + "%\n\n";
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "Adventure area completeness: ```" + str + "``` Areas with lower percentage need more events!",
                    typing: false
                });
            }
        },
        {
            cmd: "restartpc",
            params: "none",
            hidden: true,
            category: "admin",
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "This command isn't available right now.",
                    typing: false
                });
            }
        },
        {
            cmd: "lastreload",
            params: "none",
            hidden: true,
            category: "admin",
            execute: function (bot, info, args)
            {
                fs.readFile('./lastreboot.txt', function read (err, data)
                {
                    if (err)
                    {
                        data = "Unknown";
                    }

                    bot.sendMessage({
                        to: info.channelID,
                        message: data,
                        typing: false
                    });
                });
            }
        },
        {
            cmd: "logdump",
            params: "nothing",
            hidden: true,
            category: "admin",
            execute: function (bot, info, args)
            {
                var url = "plog.txt";
                //exec("type plog.txt > tlog.txt;type log.txt >> tlog.txt", function(err, stdout, stderr) 
                //{
                bot.uploadFile({
                    to: info.channelID,
                    file: "crashlog.txt"
                }, function (error, response)
                {
                    bot.uploadFile({
                        to: info.channelID,
                        file: "currentlog.txt"
                    }, function (error, response)
                    {
                        console.log(error);
                    });
                });
                //})
            }
        },
        {
            cmd: "exec",
            params: "command",
            hidden: true,
            category: "admin",
            execute: function (bot, info, args)
            {
                var command = args.join(" ");

                var exec = require('child_process').exec;
                exec(command, function (err, stdout, stderr) 
                {
                    console.log(stdout);
                    bot.sendMessage({
                        to: info.channelID,
                        message: "```" + stdout + "```",
                        typing: false
                    });
                });
            }
        },
        {
            cmd: "version",
            params: "none",
            category: "main",
            execute: function (bot, info, args)
            {
                var exec = require('child_process').exec;
                exec('git log --oneline -n 1 HEAD', function (err, stdout, stderr) 
                {
                    console.log(stdout);
                    bot.sendMessage({
                        to: info.channelID,
                        message: "Current version: ```" + stdout + "```",
                        typing: false
                    });
                });
            }
        },
        {
            cmd: "servertime",
            params: "none",
            category: "ffxiv",
            execute: function (bot, info, args)
            {
                var d = new Date();

                var h = (d.getUTCHours() < 10 ? "0" : "") + d.getUTCHours();
                var m = (d.getUTCMinutes() < 10 ? "0" : "") + d.getUTCMinutes();
                var s = (d.getUTCSeconds() < 10 ? "0" : "") + d.getUTCSeconds();

                var str = h + ":" + m + ":" + s;

                bot.sendMessage({
                    to: info.channelID,
                    message: "Current server time: `" + str + "`",
                    typing: true
                });
            }
        },
        {
            cmd: "teamlist",
            params: "none",
            category: "teams",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].teams = bot.data[info.serverId].teams || [];
                var teams = bot.data[info.serverId].teams;

                var lines = "";

                if (teams == null || teams.length == 0)
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "There are no teams yet! Create your own with >teamcreate.",
                        typing: false
                    });
                }
                else
                {
                    for (var i = 0; i < teams.length; i++)
                    {
                        lines += teams[i].name + ", lead by: <@!" + teams[i].leader + ">\n";
                    }

                    bot.sendMessage({
                        to: info.channelID,
                        message: lines,
                        typing: false
                    });
                }
            }
        },
        {
            cmd: "teammembers",
            params: "Team name (optional)",
            category: "teams",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].teams = bot.data[info.serverId].teams || [];
                var teams = bot.data[info.serverId].teams;

                var team = null;

                for (var t = 0; t < teams.length; t++)
                {
                    for (var m = 0; m < teams[t].members.length; m++)
                    {
                        if (args.length == 0)
                        {
                            if (teams[t].members[m] == info.userID)
                            {
                                team = teams[t];
                            }
                        }
                        else
                        {
                            if (teams[t].name.toLowerCase() == args.join(" ").toLowerCase())
                            {
                                team = teams[t];
                            }
                        }
                    }
                }

                for (var i = 0; i < teams.length; i++)
                {
                    var str = "Lead by: <@!" + team.leader + ">\nMembers: ";

                    for (var m = 0; m < team.members.length; m++)
                    {
                        if (team.leader != team.members[m])
                            str += "<@!" + team.members[m] + ">\n";
                    }

                    bot.sendMessage({
                        to: info.channelID,
                        message: str,
                        typing: false
                    });

                    return;
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "Team '" + teamName + "' not found.",
                    typing: false
                });
            }
        },
        {
            cmd: "teamleave",
            params: "none",
            category: "teams",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].teams = bot.data[info.serverId].teams || [];
                var teams = bot.data[info.serverId].teams;

                for (var i = 0; i < teams.length; i++)
                {
                    if (teams[i].leader == info.userID)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "You can't leave a team while being the leader! Promote someone else first.",
                            typing: false
                        });

                        return;
                    }
                    else
                    {
                        for (var m = 0; m < teams[i].members.length; m++)
                        {
                            if (teams[i].members[m] == info.userID)
                            {
                                bot.sendMessage({
                                    to: info.channelID,
                                    message: "You left " + teams[i].name,
                                    typing: false
                                });

                                teams[i].members.splice(m, 1);
                                bot.data[info.serverId].teams = teams;
                                return;
                            }
                        }
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "You're not in a team!",
                    typing: false
                });
            }
        },
        {
            cmd: "teamkick",
            params: "@user to kick",
            category: "teams",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].teams = bot.data[info.serverId].teams || [];
                var teams = bot.data[info.serverId].teams;

                var validMention = args[0].match(/(<@)!?([0-9]+)(>)/);
                if (!validMention)
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "Invalid target.",
                        typing: false
                    });
                    return;
                }

                var mention = validMention[2];

                if (validMention)
                {
                    for (var i = 0; i < teams.length; i++)
                    {
                        if (teams[i].leader == info.userID)
                        {
                            for (var m = 0; m < teams[i].members.length; m++)
                            {
                                if (teams[i].members[m] == mention)
                                {
                                    bot.sendMessage({
                                        to: info.channelID,
                                        message: "<@!" + mention + "> was removed from " + teams[i].name,
                                        typing: false
                                    });

                                    teams[i].members.splice(m, 1);
                                    bot.data[info.serverId].teams = teams;
                                    return;
                                }
                            }

                            bot.sendMessage({
                                to: info.channelID,
                                message: "<@!" + mention + "> isn't in " + teams[i].name,
                                typing: false
                            });

                            return;
                        }
                        else
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: "You're not the leader of this team.",
                                typing: false
                            });

                            return;
                        }
                    }

                    bot.sendMessage({
                        to: info.channelID,
                        message: "You're not in a team!",
                        typing: false
                    });
                }
            }
        },
        {
            cmd: "teampromote",
            params: "@user to promote",
            category: "teams",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].teams = bot.data[info.serverId].teams || [];
                var teams = bot.data[info.serverId].teams;

                var validMention = args[0].match(/(<@)!?([0-9]+)(>)/);
                if (!validMention)
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "Invalid target.",
                        typing: false
                    });
                    return;
                }

                var mention = validMention[2];

                if (validMention)
                {
                    for (var i = 0; i < teams.length; i++)
                    {
                        if (teams[i].leader == info.userID)
                        {
                            for (var m = 0; m < teams[i].members.length; m++)
                            {
                                if (teams[i].members[m] == mention)
                                {
                                    teams[i].leader = teams[i].members[m];
                                    bot.data[info.serverId].teams = teams;

                                    bot.sendMessage({
                                        to: info.channelID,
                                        message: "<@!" + teams[i].leader + "> is now the leader of team " + teams[i].name + "!",
                                        typing: false
                                    });

                                    return;
                                }
                            }

                            bot.sendMessage({
                                to: info.channelID,
                                message: "<@!" + mention + "> isn't in this team!",
                                typing: false
                            });

                            return;
                        }
                    }

                    bot.sendMessage({
                        to: info.channelID,
                        message: "You're not the leader of a team!",
                        typing: false
                    });
                }
            }
        },
        {
            cmd: "teaminvite",
            params: "@user to invite",
            category: "teams",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].teams = bot.data[info.serverId].teams || [];
                var teams = bot.data[info.serverId].teams;

                var validMention = args[0].match(/(<@)!?([0-9]+)(>)/);
                if (!validMention)
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "Invalid target.",
                        typing: false
                    });
                    return;
                }

                var mention = validMention[2];

                if (validMention)
                {
                    bot.data[info.serverId].adventurers = bot.data[info.serverId].adventurers || {};
                    var target = bot.data[info.serverId].adventurers[mention] || null;

                    if (target == null)
                    {
                        target =
                        {
                            invites: []
                        };
                    }

                    for (var i = 0; i < teams.length; i++)
                    {
                        if (teams[i].leader == info.userID)
                        {
                            for (var m = 0; m < teams[i].members.length; m++)
                            {
                                if (teams[i].members[m] == mention)
                                {
                                    bot.sendMessage({
                                        to: info.channelID,
                                        message: "<@!" + mention + "> is already a member!",
                                        typing: false
                                    });

                                    return;
                                }
                            }

                            bot.sendMessage({
                                to: info.channelID,
                                message: "<@!" + mention + "> has been invited to join " + teams[i].name + "!",
                                typing: false
                            });

                            target.invites.push(teams[i].name);
                            bot.data[info.serverId].adventurers[mention] = target;
                            return;
                        }
                    }

                    bot.sendMessage({
                        to: info.channelID,
                        message: "You're not the leader of a team!",
                        typing: false
                    });
                }
            }
        },
        {
            cmd: "teamjoin",
            params: "Team to join",
            category: "teams",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].adventurers = bot.data[info.serverId].adventurers || {};
                var me = bot.data[info.serverId].adventurers[info.userID];

                bot.data[info.serverId].teams = bot.data[info.serverId].teams || [];
                var teams = bot.data[info.serverId].teams;

                var teamName = args.join(" ");

                var canJoin = false;

                if (me == null)
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "You haven't been invited to that team.",
                        typing: false
                    });

                    return;
                }
                else
                {
                    for (var i = 0; i < me.invites.length; i++)
                    {
                        if (me.invites[i].toLowerCase() == teamName.toLowerCase())
                        {
                            canJoin = true;
                        }
                    }
                }

                for (var i = 0; i < teams.length; i++)
                {
                    if (teams[i].leader == info.userID)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "You can't join a team while being the leader of an existing team!",
                            typing: false
                        });

                        canJoin = false;
                    }
                    else
                    {
                        for (var m = 0; m < teams[i].members.length; m++)
                        {
                            if (teams[i].members[m] == info.userID)
                            {
                                teams[i].members.splice(m, 1);
                                bot.sendMessage({
                                    to: info.channelID,
                                    message: "You left " + teams[i].teamName,
                                    typing: false
                                });
                            }
                        }
                    }
                }

                if (canJoin)
                {
                    for (var i = 0; i < teams.length; i++)
                    {
                        if (teams[i].name.toLowerCase() == teamName.toLowerCase())
                        {
                            me.invites = [];
                            bot.sendMessage({
                                to: info.channelID,
                                message: "You joined " + teams[i].name + "!",
                                typing: false
                            });

                            teams[i].members.push(info.userID);
                            bot.data[info.serverId].teams = teams;
                            bot.data[info.serverId].adventurers[info.userID] = me;
                            return;
                        }
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "You haven't been invited to that team.",
                    typing: false
                });
            }
        },
        {
            cmd: "teamdisband",
            params: "none (warning: deletes storage)",
            category: "teams",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].teams = bot.data[info.serverId].teams || [];
                var teams = bot.data[info.serverId].teams;

                for (var i = 0; i < teams.length; i++)
                {
                    if (teams[i].leader == info.userID)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: teams[i].name + " has been disbanded.",
                            typing: false
                        });

                        teams.splice(i, 1);

                        bot.data[info.serverId].teams = teams;
                        return;
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "You're not the leader of a team!",
                    typing: false
                });
            }
        },
        {
            cmd: "teamdeposit",
            params: "quantity(optional), item name",
            category: "teams",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].teams = bot.data[info.serverId].teams || [];
                var teams = bot.data[info.serverId].teams;
                var items = bot.data[info.serverId].inventory[info.userID] || [];

                for (var t = 0; t < teams.length; t++)
                {
                    for (var m = 0; m < teams[t].members.length; m++)
                    {
                        if (teams[t].members[m] == info.userID)
                        {
                            var quantity = args[0].match(/[0-9]+|all|All|ALL/);

                            if (quantity)
                            {
                                args.splice(0, 1);
                            }
                            else
                            {
                                quantity = 1;
                            }

                            if (quantity <= 0)
                            {
                                bot.sendMessage({
                                    to: info.channelID,
                                    message: "You store nothing.",
                                    typing: false
                                });

                                return;
                            }
                            else
                            {
                                var name = args.join(" ").trim();

                                var found = false;
                                var properName = name;

                                for (var i = 0; i < items.length; i++)
                                {
                                    items[i] = fixItem(items[i]);

                                    if (items[i].name.toLowerCase() == name.toLowerCase() || (items[i].plural || "").toLowerCase() == name.toLowerCase())
                                    {
                                        if (quantity.toString().toLowerCase() == "all")
                                        {
                                            quantity = items[i].quantity;
                                        }

                                        if (items[i].quantity >= quantity)
                                        {
                                            found = true;
                                            properName = items[i].name;
                                            items[i].quantity -= quantity;

                                            var toGive = JSON.parse(JSON.stringify(items[i]));
                                            toGive.quantity = quantity;

                                            items = removeEmptyFromInventory(items);

                                            bot.data[info.serverId].inventory[info.userID] = items;

                                            var existingFound = false;

                                            for (var o = 0; o < teams[t].storage.length; o++)
                                            {
                                                if (teams[t].storage[o].name.toLowerCase() == name.toLowerCase() || (teams[t].storage[o].plural || "").toLowerCase() == name.toLowerCase())
                                                {
                                                    existingFound = true;
                                                    teams[t].storage[o].quantity -= -quantity;
                                                }
                                            }

                                            if (!existingFound)
                                                teams[t].storage.push(toGive);
                                        }
                                        break;
                                    }
                                }

                                var item = findItemByName(properName);

                                var itemName = (quantity == 1 ? item.name : item.plural);

                                if (found)
                                {
                                    bot.sendMessage({
                                        to: info.channelID,
                                        message: "Storing " + (quantity == 1 ? item.an.toLowerCase() : quantity) + " " + itemName + " in " + teams[t].name + " storage.",
                                        typing: false
                                    });
                                }
                                else
                                {
                                    bot.sendMessage({
                                        to: info.channelID,
                                        message: "You don't have " + (quantity == 1 ? item.an.toLowerCase() : quantity) + " " + itemName + ".",
                                        typing: false
                                    });
                                }

                                return;
                            }
                        }
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "You're not a member of a team!",
                    typing: false
                });
            }
        },
        {
            cmd: "teamwithdraw",
            params: "quantity(optional), item name",
            category: "teams",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].teams = bot.data[info.serverId].teams || [];
                var teams = bot.data[info.serverId].teams;

                for (var t = 0; t < teams.length; t++)
                {
                    for (var m = 0; m < teams[t].members.length; m++)
                    {
                        if (teams[t].members[m] == info.userID)
                        {
                            var items = teams[t].storage || [];

                            var quantity = args[0].match(/[0-9]+|all|All|ALL/);

                            if (quantity)
                            {
                                args.splice(0, 1);
                            }
                            else
                            {
                                quantity = 1;
                            }

                            if (quantity <= 0)
                            {
                                bot.sendMessage({
                                    to: info.channelID,
                                    message: "You withdraw nothing.",
                                    typing: false
                                });

                                return;
                            }
                            else
                            {
                                var name = args.join(" ").trim();

                                var found = false;
                                var properName = name;

                                for (var i = 0; i < items.length; i++)
                                {
                                    items[i] = fixItem(items[i]);

                                    if (items[i].name.toLowerCase() == name.toLowerCase() || (items[i].plural || "").toLowerCase() == name.toLowerCase())
                                    {
                                        if (quantity.toString().toLowerCase() == "all")
                                        {
                                            quantity = items[i].quantity;
                                        }

                                        if (items[i].quantity >= quantity)
                                        {
                                            found = true;
                                            properName = items[i].name;
                                            items[i].quantity -= quantity;

                                            var toGive = JSON.parse(JSON.stringify(items[i]));
                                            toGive.quantity = quantity;

                                            items = removeEmptyFromInventory(items);

                                            teams[t].storage = items;

                                            var existingFound = false;

                                            for (var o = 0; o < bot.data[info.serverId].inventory[info.userID].length; o++)
                                            {
                                                if (bot.data[info.serverId].inventory[info.userID][o].name.toLowerCase() == name.toLowerCase() || (bot.data[info.serverId].inventory[info.userID][o].plural || "").toLowerCase() == name.toLowerCase())
                                                {
                                                    existingFound = true;
                                                    bot.data[info.serverId].inventory[info.userID][o].quantity -= -quantity;
                                                }
                                            }

                                            if (!existingFound)
                                                bot.data[info.serverId].inventory[info.userID].push(toGive);
                                        }
                                        break;
                                    }
                                }

                                var item = findItemByName(properName);

                                var itemName = (quantity == 1 ? item.name : item.plural);

                                if (found)
                                {
                                    bot.sendMessage({
                                        to: info.channelID,
                                        message: "Taking " + (quantity == 1 ? item.an.toLowerCase() : quantity) + " " + itemName + " from " + teams[t].name + " storage.",
                                        typing: false
                                    });
                                }
                                else
                                {
                                    bot.sendMessage({
                                        to: info.channelID,
                                        message: "Storage doesn't contain " + (quantity == 1 ? item.an.toLowerCase() : quantity) + " " + itemName + ".",
                                        typing: false
                                    });
                                }

                                return;
                            }
                        }
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "You're not a member of a team!",
                    typing: false
                });
            }
        },
        {
            cmd: "teamcreate",
            params: "team name",
            category: "teams",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].adventurers = bot.data[info.serverId].adventurers || {};
                var me = bot.data[info.serverId].adventurers[info.userID];

                bot.data[info.serverId].teams = bot.data[info.serverId].teams || [];
                var teams = bot.data[info.serverId].teams;

                var teamName = args.join(" ");

                if (me == null)
                {
                    me =
                    {
                        invites: []
                    };
                }

                bot.data[info.serverId].adventurers[info.userID] = me;

                var canMake = true;

                for (var i = 0; i < teams.length; i++)
                {
                    if (teams[i].leader == info.userID)
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: "You can't create a team while being the leader of an existing team!",
                            typing: false
                        });

                        canMake = false;
                    }
                    else
                    {
                        for (var m = 0; m < teams[i].members.length; m++)
                        {
                            if (teams[i].members[m] == info.userID)
                            {
                                teams[i].members.splice(m, 1);
                                bot.sendMessage({
                                    to: info.channelID,
                                    message: "You left " + teams[i].name,
                                    typing: false
                                });
                            }
                        }
                    }
                }

                if (canMake)
                {
                    for (var i = 0; i < teams.length; i++)
                    {
                        if (teams[i].name.toLowerCase() == teamName.toLowerCase())
                        {
                            canMake = false;
                            bot.sendMessage({
                                to: info.channelID,
                                message: "Team " + teamName + " already exists! Ask <@!" + teams[i].leader + "> if you can join.",
                                typing: false
                            });
                        }
                    }
                }

                if (canMake)
                {
                    var newTeam =
                    {
                        name: teamName,
                        leader: info.userID,
                        members: [
                            info.userID
                        ],
                        storage: []
                    };

                    teams.push(newTeam);
                    bot.data[info.serverId].teams = teams;

                    bot.sendMessage({
                        to: info.channelID,
                        message: "Team " + teamName + " created!",
                        typing: false
                    });
                }
            }
        },
        {
            cmd: "teamstorage",
            params: "(optional) team name",
            category: "teams",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].teams = bot.data[info.serverId].teams || [];
                var teams = bot.data[info.serverId].teams;

                var team = null;

                for (var t = 0; t < teams.length; t++)
                {
                    for (var m = 0; m < teams[t].members.length; m++)
                    {
                        if (args.length == 0)
                        {
                            if (teams[t].members[m] == info.userID)
                            {
                                team = teams[t];
                            }
                        }
                        else
                        {
                            if (teams[t].name.toLowerCase() == args.join(" ").toLowerCase())
                            {
                                team = teams[t];
                            }

                        }
                    }
                }

                var items = team.storage;

                var lines = [];
                var totalValue = 0;

                items.sort(function (a, b)
                {
                    return (b.quantity * b.value) - (a.quantity * a.value);
                });

                for (var i = 0; i < items.length; i++)
                {
                    var dashes = "";
                    var spaces = "";

                    items[i] = fixItem(items[i]);

                    var itemDat = findItemByName(items[i].name);
                    var itemName = (items[i].quantity == 1 ? itemDat.name : itemDat.plural);

                    for (var a = 0; a < 25 - (itemName.length + items[i].quantity.toString().length); a++)
                        dashes += "-";

                    if (items[i].name == "Stock")
                        items[i].value = currentStockPrice(bot, info);

                    for (var a = 0; a < 6 - (items[i].value).toString().length; a++)
                        spaces += " ";

                    lines.push(items[i].quantity + " " + itemName + " " + dashes + " value: " + (items[i].value) + spaces + "(" + (items[i].value * items[i].quantity) + " total)");
                    totalValue += (items[i].value * items[i].quantity);
                }

                if (lines.length == 0)
                    lines.push("Nothing!");

                bot.sendMessage({
                    to: info.channelID,
                    message: team.name + " Total Storage Value: " + totalValue + " ```" + lines.join("\n") + "```",
                    typing: false
                });
            }
        },
        {
            cmd: "teamrankings",
            params: "none",
            category: "teams",
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].teams = bot.data[info.serverId].teams || [];
                bot.data[info.serverId].inventory = bot.data[info.serverId].inventory || {};

                var teams = bot.data[info.serverId].teams;

                var ranks = [];

                for (var i = 0; i < teams.length; i++)
                {
                    var totalValue = 0;

                    bot.data[info.serverId].inventory[teams[i].leader] = bot.data[info.serverId].inventory[teams[i].leader] || [];
                    var memberValue = getInventoryTotalValue(bot.data[info.serverId].inventory[teams[i].leader], bot, info);

                    for (var m = 0; m < teams[i].members.length; m++)
                    {
                        bot.data[info.serverId].inventory[teams[i].members[m]] = bot.data[info.serverId].inventory[teams[i].members[m]] || [];
                        memberValue += getInventoryTotalValue(bot.data[info.serverId].inventory[teams[i].members[m]], bot, info);
                    }

                    totalValue += Math.floor(memberValue * 0.01);

                    var items = teams[i].storage;

                    for (var it = 0; it < items.length; it++)
                    {
                        if (items[it].name == "Stock")
                        {
                            items[it].value = currentStockPrice(bot, info);
                        }

                        totalValue += (items[it].value * items[it].quantity);
                    }

                    ranks.push({ id: teams[i].name, leader: teams[i].leader, val: totalValue });
                }

                ranks.sort(function (a, b) { return b.val - a.val; });

                if (ranks.length > 10)
                    ranks.splice(10, ranks.length - 10);

                var str = "Top 10 Teams: \n";

                for (var i = 0; i < ranks.length; i++)
                {
                    str += (i + 1) + ": " + (i < 9 ? " " : "") + ranks[i].id + ", lead by <@!" + ranks[i].leader + ">, valued at " + ranks[i].val + (ranks[i].val == 1 ? " coins!\n" : " coins!\n");
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: str,
                    typing: false
                });
            }
        }
    ];

function adventureStep (bot, info, group, asMention, serverId, areaName, count, acount)
{
    var maxSteps = group.length <= 1 ? 10 : ((group.length - 1) * 5) + 10;

    var areaIndex = -1;

    for (var i = 0; i < adventures.length; i++)
    {
        if (adventures[i].name == areaName)
        {
            areaIndex = i;
            break;
        }
    }

    bot.data.lastAdventure = bot.data.lastAdventure || {};

    var previous = bot.data.lastAdventure;

    bot.data.lastAdventure.index = i;
    bot.data.lastAdventure.areaName = areaName;
    bot.data.lastAdventure.step = "bad";
    bot.data.lastAdventure.stepIndex = -1;

    bot.saveData();

    genderData = RandomGenderData();
    bot.data[serverId].inventory = bot.data[serverId].inventory || {};

    count++;
    acount++;
    var sI = -1;
    var areaApproved = false;
    var step = null;

    var tries = 0;

    while (!areaApproved)
    {
        tries++;
        areaApproved = true;
        sI = Math.round(Math.random() * (adventures[areaIndex].events.length - 1));
        step = JSON.parse(JSON.stringify(adventures[areaIndex].events[sI]));

        if (tries < 30)
        {
            if (count < 7)
            {
                if (step.forceEnd)
                    areaApproved = false;
            }

            if (count < 5)
            {
                if (step.killMember)
                    areaApproved = false;
            }

            if (acount < 5)
            {
                if (step.toArea != null)
                    areaApproved = false;
            }
        }
    }

    bot.data.lastAdventure.stepIndex = sI;
    bot.saveData();

    console.log(areaName + " sI: " + sI);
    console.log(adventures[areaIndex].events[sI]);

    if (sI < 0)
        sI = 0;

    var takeStr = "";
    var lostStr = "";

    var exceptionActive = false;
    var exceptionStr = "";

    var memberIDs = [];

    for (var i = 0; i < group.length; i++)
    {
        memberIDs.push(i);
        shuffle(memberIDs);
    }

    var blockExceptions = false;

    var baseMsg = group.length <= 1 ? step.descriptionSolo : step.descriptionMulti;

    bot.data.lastAdventure.step = baseMsg;

    bot.saveData();

    if (step.loot && step.loot.name !== "nothing")
    {
        var canLoot = !step.loot.conditional;

        var itemUser = null;

        console.log("Conditional loot: " + step.loot.conditional);

        if (step.loot.conditional)
        {
            if (step.loot.conditionalRequirements.items.length > 1)
                shuffle(step.loot.conditionalRequirements.items);

            for (var i = 0; i < group.length; i++)
            {
                if (canLoot)
                    break;

                console.log("checking user for requirements: " + memberIDs[i] + " -- " + group[memberIDs[i]]);
                var items = bot.data[serverId].inventory[group[memberIDs[i]]] || [];

                console.log(items);

                for (var ii = 0; ii < items.length; ii++)
                {
                    if (canLoot)
                        break;

                    for (var ci = 0; ci < step.loot.conditionalRequirements.items.length; ci++)
                    {
                        var citem = step.loot.conditionalRequirements.items[ci];

                        var item = findItemByName(citem);

                        var itemName = (step.loot.conditionalRequirements.quantity == 1 ? item.name : item.plural);
                        var itemAn = item.an;

                        console.log("item [" + ii + "], " + items[ii].name + ": " + (items[ii].name == citem));

                        if (items[ii].name == citem)
                        {
                            if (items[ii].quantity >= step.loot.conditionalRequirements.quantity)
                            {
                                if (step.loot.conditionalRequirements.consumed)
                                {
                                    items[ii].quantity -= step.loot.conditionalRequirements.quantity;
                                    removeEmptyFromInventory(items);
                                    bot.data[serverId].inventory[group[memberIDs[i]]] = items;
                                }

                                canLoot = true;
                                blockExceptions = true;
                                itemUser = memberIDs[i];

                                var usedStr = (citem == "Coin" && step.loot.conditionalRequirements.consumed) ? " spends " : (" used " + (step.loot.conditionalRequirements.consumed ? "up " : ""));

                                usedStr = step.loot.conditionalRequirements.involuntary ? " loses " : usedStr;

                                baseMsg = group.length <= 1 ? step.loot.conditionalRequirements.descriptionSolo : step.loot.conditionalRequirements.descriptionMulti;
                                exceptionStr = "\n" + asMention[memberIDs[i]] + usedStr + (step.loot.conditionalRequirements.quantity == 1 ? itemAn.toLowerCase() : step.loot.conditionalRequirements.quantity) + " " + itemName + ".";
                                break;
                            }
                        }
                    }
                }
            }
        }

        if (canLoot)
        {
            var quantity = Math.round(step.loot.minQuantity + ((step.loot.maxQuantity - step.loot.minQuantity) * Math.random()));

            var item = findItemByName(step.loot.name);

            var itemName = (quantity == 1 ? item.name : item.plural);

            if (step.loot.each)
            {
                for (var i = 0; i < group.length; i++)
                {
                    addToUserInventory(bot, group[i], findItemByName(step.loot.name), serverId, quantity);
                }

                if (group.length > 1)
                {
                    takeStr = "\n" + (group.length > 2 ? "Everyone obtains" : "You both take") + " " + (quantity == 1 ? item.an.toLowerCase() : quantity) + " " + itemName + ".";
                }
                else
                {
                    takeStr = "\n" + asMention[0] + " obtains " + (quantity == 1 ? item.an.toLowerCase() : quantity) + " " + itemName + ".";
                }
            }
            else
            {
                var lost = step.loot.conditional ? itemUser : Math.round(Math.random() * (group.length - 1));

                addToUserInventory(bot, group[lost], findItemByName(step.loot.name), serverId, quantity);

                takeStr = "\n" + asMention[lost] + " obtains " + (quantity == 1 ? item.an.toLowerCase() : quantity) + " " + itemName + ".";
            }
        }
    }

    var exceptions = step.exceptions;
    shuffle(exceptions);

    if (exceptions.length > 0 && !blockExceptions)
    {
        for (var e = 0; e < exceptions.length; e++)
        {
            if (exceptionActive)
                break;

            for (var i = 0; i < group.length; i++)
            {
                if (exceptionActive)
                    break;

                var items = bot.data[serverId].inventory[group[memberIDs[i]]] || [];

                for (var ii = 0; ii < items.length; ii++)
                {
                    if (exceptionActive)
                        break;

                    if (items[ii].name == exceptions[e].item)
                    {
                        if (items[ii].quantity >= exceptions[e].quantity)
                        {
                            if (exceptions[e].consumed)
                            {
                                items[ii].quantity -= exceptions[e].quantity;
                                removeEmptyFromInventory(items);
                                bot.data[serverId].inventory[group[memberIDs[i]]] = items;
                            }

                            var usedStr = (exceptions[e].item == "Coin" && exceptions[e].consumed) ? " spends " : (" used " + (exceptions[e].consumed ? "up " : ""));

                            usedStr = exceptions[e].involuntary ? " loses " : usedStr;

                            exceptionActive = true;

                            step.killMember = false;
                            step.forceEnd = false;

                            baseMsg = group.length <= 1 ? exceptions[e].descriptionSolo : exceptions[e].descriptionMulti;

                            var item = findItemByName(exceptions[e].item);

                            var itemName = (exceptions[e].quantity == 1 ? item.name : item.plural);

                            exceptionStr = "\n" + asMention[memberIDs[i]] + usedStr + (exceptions[e].quantity == 1 ? item.an.toLowerCase() : exceptions[e].quantity) + " " + itemName + ".";
                            break;
                        }
                    }
                }
            }
        }
    }

    if (step.killMember && !exceptionActive)
    {
        var lost = Math.round(Math.random() * (group.length - 1));

        var id = bot.data[info.serverId].adventurelock.indexOf(group[lost]);
        bot.data[info.serverId].adventurelock.splice(id, 1);

        bot.data[info.serverId].adventurelock.splice();

        group.splice(lost, 1);
        lostStr = "\n" + asMention.splice(lost, 1) + " leaves the party.";
    }

    var newArea = areaName;

    if (step.toArea != null)
    {
        var canTransfer = !step.toArea.conditional;

        if (step.toArea.conditional)
        {
            if (step.toArea.conditionalRequirements.items.length > 1)
                shuffle(step.toArea.conditionalRequirements.items);

            for (var i = 0; i < group.length; i++)
            {
                if (canTransfer)
                    break;

                console.log("checking user for requirements: " + memberIDs[i] + " -- " + group[memberIDs[i]]);
                var items = bot.data[serverId].inventory[group[memberIDs[i]]] || [];

                console.log(items);

                for (var ii = 0; ii < items.length; ii++)
                {
                    if (canTransfer)
                        break;

                    for (var ci = 0; ci < step.toArea.conditionalRequirements.items.length; ci++)
                    {
                        var citem = step.toArea.conditionalRequirements.items[ci];

                        var item = findItemByName(citem);

                        var itemName = (step.toArea.conditionalRequirements.quantity == 1 ? item.name : item.plural);
                        var itemAn = item.an;

                        console.log("item [" + ii + "], " + items[ii].name + ": " + (items[ii].name == citem));

                        if (items[ii].name == citem)
                        {
                            if (items[ii].quantity >= step.toArea.conditionalRequirements.quantity)
                            {
                                if (step.toArea.conditionalRequirements.consumed)
                                {
                                    items[ii].quantity -= step.toArea.conditionalRequirements.quantity;
                                    removeEmptyFromInventory(items);
                                    bot.data[serverId].inventory[group[memberIDs[i]]] = items;
                                }

                                canTransfer = true;
                                itemUser = memberIDs[i];

                                var usedStr = (citem == "Coin" && step.toArea.conditionalRequirements.consumed) ? " spends " : (" used " + (step.toArea.conditionalRequirements.consumed ? "up " : ""));

                                usedStr = step.toArea.conditionalRequirements.involuntary ? " loses " : usedStr;

                                baseMsg = group.length <= 1 ? step.toArea.conditionalRequirements.descriptionSolo : step.toArea.conditionalRequirements.descriptionMulti;
                                exceptionStr = "\n" + asMention[memberIDs[i]] + usedStr + (step.toArea.conditionalRequirements.quantity == 1 ? itemAn.toLowerCase() : step.toArea.conditionalRequirements.quantity) + " " + itemName + ".";
                                break;
                            }
                        }
                    }
                }
            }
        }

        if (canTransfer)
        {
            newArea = step.toArea.areaName;
            acount = 0;
        }
    }

    var nextAction = function ()
    {
        var forceEnd = step.forceEnd && !exceptionActive;

        if (forceEnd || group.length == 0 || count > maxSteps)
        {
            if (count > maxSteps && !forceEnd && group.length > 0)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "You get tired, and decide to stop for today.",
                    typing: true
                }, function ()
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "The adventure ends.",
                        typing: true
                    });
                });
            }
            else
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "The adventure ends.",
                    typing: true
                });
            }

            for (var o = 0; o < group.length; o++)
            {
                var id = bot.data[info.serverId].adventurelock.indexOf(group[o]);
                bot.data[info.serverId].adventurelock.splice(id, 1);
            }
        }
        else
        {
            adventureStep(bot, info, group, asMention, serverId, areaName, count, acount);
        }
    };

    bot.data.lastAdventure = previous;

    bot.sendMessage({
        to: info.channelID,
        message: genderData.parseStr(baseMsg) + exceptionStr + takeStr + lostStr,
        typing: true
    }, function ()
    {
        if (newArea != areaName)
        {
            areaName = newArea;

            bot.sendMessage({
                to: info.channelID,
                message: "The party enters the " + areaName + ".",
                typing: true
            }, nextAction());
        }
        else
        {
            nextAction();
        }
    });
}

function addToUserInventory (bot, userID, newItem, serverId, quantity)
{
    bot.data[serverId].inventory = bot.data[serverId].inventory || {};
    var items = bot.data[serverId].inventory[userID] || [];

    var found = false;

    for (var i = 0; i < items.length; i++)
    {
        if (items[i].name == newItem.name || items[i].plural == newItem.plural)
        {
            items[i].quantity -= -quantity;
            found = true;

            if (!items[i].plural || items[i].plural == "")
                items[i].plural = newItem.plural;

            break;
        }
    }

    if (!found)
    {
        newItem.quantity = quantity;
        items.push(newItem);
    }

    bot.data[serverId].inventory[userID] = items;
}

function parseTextDice (text)
{
    var cont = true;
    var r = /(\d+)d(\d+)/;

    while (cont)
    {
        var matches = text.match(r);

        console.log(matches);

        if (matches === null)
        {
            cont = false;
            break;
        }

        if (matches.length < 2)
        {
            cont = false;
            break;
        }

        a = matches[1] * rand(1, matches[2] - -1);

        text = text.replace(r, a);
    }

    cont = true;
    r = /(\d+)([\/|\*])(\d+)/;
    while (cont)
    {
        var matches = text.match(r);

        console.log(matches);

        if (matches === null)
        {
            cont = false;
            break;
        }

        if (matches.length < 3)
        {
            cont = false;
            break;
        }

        if (matches[2] == "*")
            a = matches[1] * matches[3];
        else
            a = matches[1] / matches[3];

        text = text.replace(r, a);
    }

    cont = true;
    r = /(\d+)([\+|\-])(\d+)/;
    while (cont)
    {
        var matches = text.match(r);

        if (matches === null)
        {
            cont = false;
            break;
        }

        if (matches.length < 3)
        {
            cont = false;
            break;
        }

        if (matches[2] == "+")
            a = matches[1] - -matches[3];
        else
            a = matches[1] - matches[3];

        text = text.replace(r, a);
    }

    return text;
}

function rand (min, max)
{
    return min + (Math.floor(Math.random() * (max - min)));
}

function removeEmptyFromInventory (inventory)
{
    for (var i = inventory.length - 1; i >= 0; i--)
    {
        if (inventory[i].quantity <= 0)
            inventory.splice(i, 1);
    }

    return inventory;
}

function findItemByName (name)
{
    for (var i = 0; i < allItems.length; i++)
    {
        if (allItems[i].name.toLowerCase() == name.toLowerCase() || allItems[i].plural.toLowerCase() == name.toLowerCase())
        {
            var newItem = JSON.parse(JSON.stringify(allItems[i]));

            return newItem;
        }
    }
}

function currentStockPrice (bot, info)
{
    var serverInteger = parseInt(info.serverId.substring(info.serverId.length - 5, 5)) + (Date.now() / 77000000);

    var wobbledNumber = 1.5 +
        (
            ((Math.sin(serverInteger * 1.6) * Math.sin(serverInteger / 2.3)) / 2) +
            (Math.cos(serverInteger * 6) * 0.1) +
            (Math.sin(serverInteger / 2) * 0.5) +
            (Math.cos(serverInteger) * 0.1)
        );

    return Math.round(wobbledNumber * 100);
}

function shuffle (a)
{
    var j, x, i;
    for (i = a.length - 1; i > 0; i--)
    {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

function fixItem (item)
{
    var defaultItem = findItemByName(item.name);
    defaultItem.quantity = item.quantity;

    return defaultItem;
}

function getInventoryTotalValue (inventory, bot, info)
{
    var items = inventory;
    var totalValue = 0;

    for (var it = 0; it < items.length; it++)
    {
        if (items[it].name == "Stock")
        {
            items[it].value = currentStockPrice(bot, info);
        }

        totalValue += (items[it].value * items[it].quantity);
    }

    return totalValue;
}

function RandomGenderData ()
{
    var male = Math.random() > 0.5;

    var names =
        [
            ["Bob", "John", "Willard", "William", "James", "Peter", "Chad", "Mike", "Richard", "Jeremy", "Tim", "Jack", "Benjamin", "Ethan", "Daniel", "Alexander", "Oliver", "Matthew", "Michael", "Christopher", "Andrew", "Nicholas", "Ryan", "Brandon", "Logan", "Kevin", "Robert", "Connor", "Kyle", "Charles", "Adam", "Alex", "Steven", "Jeremiah", "Patrick", "Luke", "Isaac", "Seth"],
            ["Juliet", "Elyssa", "Anne", "Bethany", "Elise", "Claire", "Amelia", "Emma", "Emily", "Isabella", "Elizabeth", "Olivia", "Sofia", "Hannah", "Samantha", "Abigail", "Ashley", "Sarah", "Laura", "Hailey", "Chloe", "Rachel", "Lillian", "Rebecca", "Leah", "Katherine", "Marissa"]
        ];

    var result =
    {
        name: (male ? names[0][Math.floor(Math.random() * names[0].length)] : names[1][Math.floor(Math.random() * names[1].length)]),
        heshe: (male ? "He" : "She"),
        hisher: (male ? "His" : "Her"),
        himher: (male ? "Him" : "Her"),
        manwoman: (male ? "Man" : "Woman"),

        parseStr: function (str)
        {
            return str
                .replace(/{heshe}/g, this.heshe.toLowerCase())
                .replace(/{hisher}/g, this.hisher.toLowerCase())
                .replace(/{himher}/g, this.himher.toLowerCase())
                .replace(/{manwoman}/g, this.manwoman.toLowerCase())

                .replace(/{name}/g, this.name)
                .replace(/{HeShe}/g, this.heshe)
                .replace(/{HisHer}/g, this.hisher)
                .replace(/{HimHer}/g, this.himher)
                .replace(/{ManWoman}/g, this.manwoman);
        }
    };

    return result;
}

function ErasmusText (text)
{
    var output = "";

    var words = text.split(" ");
    var outputWords = [];

    var I = false;
    var B = false;
    var effectLength = 0;
    var ready = true;

    var maxLength = 5;

    for (var w = 0; w < words.length; w++)
    {
        I = false;
        B = false;
        ready = true;

        outputWords[w] = "";
        for (var i = 0; i < words[w].length; i++)
        {
            if (Math.random() < effectLength / maxLength && ready)
            {
                if (I && words[w][i] != " ")
                {
                    outputWords[w] += "*";
                    I = false;
                }
                else if (B && words[w][i] != " ")
                {
                    outputWords[w] += "**";
                    B = false;
                }
                else
                {
                    if (Math.random() > 0.5)
                    {
                        B = true;
                        outputWords[w] += "**";
                    }
                    else
                    {
                        I = true;
                        outputWords[w] += "*";
                    }
                }

                ready = false;
                effectLength = 0;
            }
            else
            {
                ready = true;
            }

            effectLength++;
            outputWords[w] += words[w][i];
        }

        if (I)
        {
            outputWords[w] += "*";
            I = false;
        }
        else if (B)
        {
            outputWords[w] += "**";
            B = false;
        }
    }

    return outputWords.join(" ");
}