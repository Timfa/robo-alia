[
  {
    cmd: 'help',
    params: 'category',
    category: 'main',
    execute: [Function: execute]
  },
  {
    cmd: 'debughelp',
    params: 'none',
    hidden: true,
    category: 'admin',
    execute: [Function: execute]
  },
  {
    cmd: 'voiceid',
    params: 'none',
    hidden: false,
    category: 'main',
    execute: [Function: execute]
  },
  {
    cmd: 'recipe',
    params: 'item name',
    category: 'adventures',
    execute: [Function: execute]
  },
  {
    cmd: 'craft',
    params: 'item name',
    category: 'adventures',
    execute: [Function: execute]
  },
  {
    cmd: 'serverid',
    params: 'none',
    hidden: true,
    category: 'admin',
    execute: [Function: execute]
  },
  {
    cmd: 'cet',
    params: 'none',
    category: 'queries',
    execute: [Function: execute]
  },
  {
    cmd: 'invitelink',
    params: 'none',
    category: 'main',
    execute: [Function: execute]
  },
  {
    cmd: 'ping',
    params: 'none',
    category: 'main',
    execute: [Function: execute]
  },
  {
    cmd: 'update',
    params: 'none',
    hidden: true,
    category: 'admin',
    execute: [Function: execute]
  },
  {
    cmd: 'roll',
    params: 'dice string (1d20+5)',
    category: 'fun',
    execute: [Function: execute]
  },
  {
    cmd: 'pat',
    params: '@mention (optional, omit to pat me!)',
    category: 'fun',
    execute: [Function: execute]
  },
  {
    cmd: 'coronatest',
    params: '@mention (optional, omit to pat me!)',
    category: 'fun',
    hidden: true,
    execute: [Function: execute]
  },
  {
    cmd: 'coronaspread',
    params: 'enable/disable',
    hidden: true,
    category: 'admin',
    execute: [Function: execute]
  },
  {
    cmd: 'favorite',
    params: 'none',
    category: 'fun',
    execute: [Function: execute]
  },
  {
    cmd: 'echo',
    params: 'anything',
    category: 'admin',
    hidden: true,
    execute: [Function: execute]
  },
  {
    cmd: 'rawecho',
    params: 'anything',
    category: 'admin',
    hidden: true,
    execute: [Function: execute]
  },
  {
    cmd: 'vaccinate',
    params: 'anything',
    category: 'admin',
    hidden: true,
    execute: [Function: execute]
  },
  {
    cmd: 'infectlist',
    params: 'anything',
    category: 'admin',
    hidden: true,
    execute: [Function: execute]
  },
  {
    cmd: 'health',
    params: 'nothing',
    category: 'fun',
    execute: [Function: execute]
  },
  {
    cmd: 'zalgo',
    params: 'a̴͚̟n̠͆͡y͏̛͞t̏́͆h̑͋͊i̻͟͝n͍͘͠g̉́̽',
    category: 'fun',
    execute: [Function: execute]
  },
  {
    cmd: 'erasmus',
    params: 'Anything',
    category: 'fun',
    execute: [Function: execute]
  },
  {
    cmd: 'memdump',
    params: 'none',
    hidden: true,
    category: 'admin',
    execute: [Function: execute]
  },
  {
    cmd: 'globalmemdump',
    params: 'none',
    hidden: true,
    category: 'admin',
    execute: [Function: execute]
  },
  {
    cmd: 'clearlocalmem',
    params: 'none',
    hidden: true,
    category: 'admin',
    execute: [Function: execute]
  },
  {
    cmd: 'clearglobalmem',
    params: 'none (untested)',
    hidden: true,
    category: 'admin',
    execute: [Function: execute]
  },
  {
    cmd: 'encode',
    params: 'anything',
    category: 'fun',
    execute: [Function: execute]
  },
  {
    cmd: 'decode',
    params: 'anything',
    category: 'fun',
    execute: [Function: execute]
  },
  {
    cmd: 'play',
    params: "'playing' string",
    category: 'meta',
    execute: [Function: execute]
  },
  {
    cmd: 'stream',
    params: "'streaming' string",
    category: 'meta',
    execute: [Function: execute]
  },
  {
    cmd: 'listento',
    params: "'listening to' string",
    category: 'meta',
    execute: [Function: execute]
  },
  {
    cmd: 'watch',
    params: "'watching' string",
    category: 'meta',
    execute: [Function: execute]
  },
  {
    cmd: 'google',
    params: 'search query',
    category: 'queries',
    execute: [Function: execute]
  },
  {
    cmd: 'inventory',
    params: 'none',
    category: 'adventures',
    execute: [Function: execute]
  },
  {
    cmd: 'summon',
    params: 'quantity(optional), item name',
    hidden: true,
    category: 'adventures',
    execute: [Function: execute]
  },
  {
    cmd: 'shop',
    params: 'none',
    category: 'adventures',
    execute: [Function: execute]
  },
  {
    cmd: 'price',
    params: 'item name',
    category: 'adventures',
    execute: [Function: execute]
  },
  {
    cmd: 'buy',
    params: 'quantity(optional), item name',
    category: 'adventures',
    execute: [Function: execute]
  },
  {
    cmd: 'sell',
    params: 'quantity(optional), item name',
    category: 'adventures',
    execute: [Function: execute]
  },
  {
    cmd: 'give',
    params: '@mention target, quantity(optional), item name',
    category: 'adventures',
    execute: [Function: execute]
  },
  {
    cmd: 'infect',
    params: '@mention target',
    category: 'admin',
    hidden: true,
    execute: [Function: execute]
  },
  {
    cmd: 'discard',
    params: 'quantity(optional), item name',
    category: 'adventures',
    execute: [Function: execute]
  },
  {
    cmd: 'resetadventurelock',
    params: 'None',
    category: 'admin',
    hidden: true,
    execute: [Function: execute]
  },
  {
    cmd: 'adventure',
    params: '@mention companions (optional, any number)',
    category: 'adventures',
    execute: [Function: execute]
  },
  {
    cmd: 'adventuredebug',
    params: 'none',
    category: 'admin',
    hidden: true,
    execute: [Function: execute]
  },
  {
    cmd: 'teamadventure',
    params: 'None',
    category: 'teams',
    execute: [Function: execute]
  },
  {
    cmd: 'forcebackup',
    params: 'nothing',
    category: 'admin',
    hidden: true,
    execute: [Function: execute]
  },
  {
    cmd: 'rankings',
    params: 'none',
    category: 'adventures',
    execute: [Function: execute]
  },
  {
    cmd: 'sadness',
    params: 'nothing',
    category: 'fun',
    execute: [Function: execute]
  },
  {
    cmd: 'givefile',
    params: 'nothing',
    category: 'admin',
    hidden: true,
    execute: [Function: execute]
  },
  {
    cmd: 'translate',
    params: 'target language code (OPTIONAL), source text',
    category: 'queries',
    execute: [Function: execute]
  },
  {
    cmd: 'ask',
    params: 'whatever you want to know!',
    category: 'queries',
    execute: [Function: execute]
  },
  {
    cmd: 'topmentions',
    params: 'none',
    category: 'main',
    execute: [Function: execute]
  },
  {
    cmd: 'adventureareas',
    params: 'none',
    category: 'adventures',
    execute: [Function: execute]
  },
  {
    cmd: 'restartpc',
    params: 'none',
    hidden: true,
    category: 'admin',
    execute: [Function: execute]
  },
  {
    cmd: 'lastreload',
    params: 'none',
    hidden: true,
    category: 'admin',
    execute: [Function: execute]
  },
  {
    cmd: 'logdump',
    params: 'nothing',
    hidden: true,
    category: 'admin',
    execute: [Function: execute]
  },
  {
    cmd: 'exec',
    params: 'command',
    hidden: true,
    category: 'admin',
    execute: [Function: execute]
  },
  {
    cmd: 'version',
    params: 'none',
    category: 'main',
    execute: [Function: execute]
  },
  {
    cmd: 'servertime',
    params: 'none',
    category: 'ffxiv',
    execute: [Function: execute]
  },
  {
    cmd: 'teamlist',
    params: 'none',
    category: 'teams',
    execute: [Function: execute]
  },
  {
    cmd: 'teammembers',
    params: 'Team name (optional)',
    category: 'teams',
    execute: [Function: execute]
  },
  {
    cmd: 'teamleave',
    params: 'none',
    category: 'teams',
    execute: [Function: execute]
  },
  {
    cmd: 'teamkick',
    params: '@user to kick',
    category: 'teams',
    execute: [Function: execute]
  },
  {
    cmd: 'teampromote',
    params: '@user to promote',
    category: 'teams',
    execute: [Function: execute]
  },
  {
    cmd: 'teaminvite',
    params: '@user to invite',
    category: 'teams',
    execute: [Function: execute]
  },
  {
    cmd: 'teamjoin',
    params: 'Team to join',
    category: 'teams',
    execute: [Function: execute]
  },
  {
    cmd: 'teamdisband',
    params: 'none (warning: deletes storage)',
    category: 'teams',
    execute: [Function: execute]
  },
  {
    cmd: 'teamdeposit',
    params: 'quantity(optional), item name',
    category: 'teams',
    execute: [Function: execute]
  },
  {
    cmd: 'teamwithdraw',
    params: 'quantity(optional), item name',
    category: 'teams',
    execute: [Function: execute]
  },
  {
    cmd: 'teamcreate',
    params: 'team name',
    category: 'teams',
    execute: [Function: execute]
  },
  {
    cmd: 'teamstorage',
    params: '(optional) team name',
    category: 'teams',
    execute: [Function: execute]
  },
  {
    cmd: 'teamrankings',
    params: 'none',
    category: 'teams',
    execute: [Function: execute]
  }
]
initializing...
savedata function defined
onready defined
onmessage defined
