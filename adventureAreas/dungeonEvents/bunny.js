exports.event = 
{
    descriptionSolo : "You encounter a bunny and decide to take it with you.",
    descriptionMulti : "Your party encounters a bunny. One of the party members decides to take it along.",

    loot:
    {
            name: "Bunny",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}