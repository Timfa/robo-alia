exports.event = 
{
    descriptionSolo : "As you move through the dungeon, you accidentally activate a trap! You fall down a hole, taking a long time to climb out. After this, you decide to stop for today.",
    descriptionMulti : "As the party moves through the dungeon, a trap is activated! One of your party members falls down a hole!",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: true,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Rope",
            consumed: true,
            quantity: 1,
            descriptionSolo: "As you move through the dungeon, you accidentally activate a trap! You fall down a hole, but manage to use your Rope to climb out again.",
            descriptionMulti: "As the party moves through the dungeon, a trap is activated! One of your party members falls down a hole! Fortunately, the party can use a Rope to save the fallen member."
        },
        {
            item: "Spellbook",
            consumed: false,
            quantity: 1,
            descriptionSolo: "As you move through the dungeon, you accidentally activate a trap! A hole forms below you, but as you fall you cast a spell to safely escape.",
            descriptionMulti: "As the party moves through the dungeon, a trap is activated! One of your party members falls down a hole! Fortunately, another party member casts a spell to save the fallen member."
        },
        {
            item: "Good Luck Charm",
            consumed: false,
            quantity: 1,
            descriptionSolo: "As you move through the dungeon, you accidentally activate a trap! A hole forms below you, but the trap malfunctions allowing you to jump out of the way.",
            descriptionMulti: "As the party moves through the dungeon, a trap is activated! A hole forms below one of your party members, but the trap malfunctions allowing the would-be victim to jump out of the way."
        }
    ],
    toArea: null
}