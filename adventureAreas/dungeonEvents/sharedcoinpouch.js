exports.event =
{
    descriptionSolo : "You find a pouch of coins!",
    descriptionMulti : "You find a pouch of coins and divide it amongst your party!",

    loot:
    {
        name: "Coin",
        each: true,
        minQuantity: 5,
        maxQuantity: 20,
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 1,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}