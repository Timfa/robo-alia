exports.event = 
{
    descriptionSolo : "You encounter a hole in a collapsed dungeon wall revealing a cavern. You move through, finding another path on the other side.",
    descriptionMulti : "You encounter a hole in a collapsed dungeon wall revealing a cavern. You move through, finding another path on the other side.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}