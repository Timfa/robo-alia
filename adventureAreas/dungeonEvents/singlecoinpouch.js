exports.event = 
{
    descriptionSolo : "You find a pouch of coins!",
    descriptionMulti : "One of your party members finds a pouch of coins! Since nobody noticed, the party member pockets it all.",

    loot:
    {
            name: "Coin",
            each: false,
            minQuantity: 10,
            maxQuantity: 30,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}