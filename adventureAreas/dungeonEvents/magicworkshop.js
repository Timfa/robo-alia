exports.event = 
{
    descriptionSolo : "You enter what appears to be an old magical workshop.\nMost of the things here are ruined, except for a single unknown magical potion.",
    descriptionMulti : "You enter what appears to be an old magical workshop.\nMost of the things here are ruined, except for a single unknown magical potion.",

    loot:
    {
            name: "Magic Potion",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}