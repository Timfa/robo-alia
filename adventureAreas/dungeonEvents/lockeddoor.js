exports.event = 
{
    descriptionSolo : "You find a locked door. Unable to find a key, so unable to continue, you end your adventure.",
    descriptionMulti : "You find a locked door. Unable to find a key, so unable to continue, you end your adventure.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: true,
    exceptions: 
    [
        {
            item: "Lockpicking Set",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You find a locked door. Fortunately, you manage to pick the lock and continue.",
            descriptionMulti: "You find a locked door. One of your party members manages to pick the lock, allowing you to continue."
        },
        {
            item: "Spellbook",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You find a locked door. You cast a spell and the lock stops existing.",
            descriptionMulti: "You find a locked door. A party member casts a spell and the lock stops existing."
        },
        {
            item: "Magic Wand",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You find a locked door. You take out your wand and blow the door from its frame.",
            descriptionMulti: "You find a locked door. A party member takes out a wand and blows the door from its frame."
        }
    ],
    toArea: null
}