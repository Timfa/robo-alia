exports.event = 
{
    descriptionSolo : "You find an old mining shaft. It hasn't been used for a long time and doesn't look safe to enter.",
    descriptionMulti : "You find an old mining shaft. It hasn't been used for a long time and doesn't look safe to enter.",

    loot:
    {
            name: "Iron Ore",
            each: true,
            minQuantity: 3,
            maxQuantity: 20,
            conditional: true,
            conditionalRequirements:
            {
                items: ["Pickaxe"],
                consumed: false,
                quantity: 1,
                descriptionSolo: "You find an old mining shaft. It hasn't been used for a long time and doesn't look safe to enter. Using a Pickaxe, you collect some ore near the entrance.",
                descriptionMulti: "You find an old mining shaft. It hasn't been used for a long time and doesn't look safe to enter. Using a Pickaxe, a party member gathers some Iron Ore near the entrance."
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}