exports.event = 
{
    descriptionSolo : "You meet a lone adventurer. You chat for a while and go on your separate ways.",
    descriptionMulti : "You meet a lone adventurer. You chat for a while and go on your separate ways.",

    loot:
    {
        name: "Mushroom Soup",
        each: false,
        minQuantity: 1,
        maxQuantity: 1,
        conditional: true,
        conditionalRequirements:
        {
            items: ["Mushroom"],
            consumed: true,
            quantity: 5,
            descriptionSolo: "You meet a lone adventurer. As you chat, {heshe} mentions having some cooking supplies with {himher}. {HeShe} lets you borrow them to make some soup.",
            descriptionMulti: "You meet a lone adventurer. As you chat, {heshe} mentions having some cooking supplies with {himher}. {HeShe} lets one of your party members borrow them to make some soup."
        }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [ ],
    toArea: null
}