exports.event = 
{
    descriptionSolo : "You meet a friendly wizard. Introducing {himher}self as {name}, {heshe} accompanies you for a moment and then heads in another direction.",
    descriptionMulti : "You meet a friendly wizard. Introducing {himher}self as {name}, {heshe} accompanies you for a moment and then heads in another direction.",

    loot:
    {
        name: "Coin",
        each: false,
        minQuantity: 40,
        maxQuantity: 80,
        conditional: true,
        conditionalRequirements:
        {
            items: ["Magic Potion"],
            consumed: true,
            quantity: 1,
            descriptionSolo: "You meet a friendly wizard called {name}. As you talk to {himher}, you mention a potion you found you have been unable to identify. {name} takes a look and offers to buy it from you. You accept and {heshe} hands you a generous sum for it.",
            descriptionMulti: "You meet a friendly wizard called {name}. As you talk to {himher}, a party member mentions having found a potion that nobody has been able to identify. {name} takes a look and offers to buy it. The party member accepts and {name} pays a generous sum for it."
        }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [ ],
    toArea: null
}