exports.event = 
{
    descriptionSolo : "You encounter a group of skeletons. Fortunately, it seems another group of adventurers got to them first as they are already defeated.",
    descriptionMulti : "You encounter a group of skeletons. Fortunately, it seems another group of adventurers got to them first as they are already defeated.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}