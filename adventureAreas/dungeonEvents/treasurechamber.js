exports.event = 
{
    descriptionSolo : "After exploring an old chamber you discover a hidden passage. Entering what appears to be a treasure chamber, you uncover an ancient relic.",
    descriptionMulti : "After exploring an old chamber one of your party members discovers a hidden passage. Entering what appears to be a treasure chamber, you uncover an ancient relic.",

    loot:
    {
        name: "Ancient Relic",
        each: false,
        minQuantity: 1,
        maxQuantity: 1,
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 1,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}