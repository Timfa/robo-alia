exports.event = 
{
    descriptionSolo : "You walk into a pitch-black chamber. Eventually you find a way out, only to find yourself back at the dungeon entrance.",
    descriptionMulti : "You walk into a pitch-black chamber. When you leave, you notice one of your members has gone missing.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: true,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Torch",
            consumed: true,
            quantity: 1,
            descriptionSolo: "You walk into a pitch-black chamber. Lighting up a torch, you navigate through easily.",
            descriptionMulti: "You walk into a pitch-black chamber. A party member lights up a torch and you navigate through easily."
        },
        {
            item: "Fairy",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You walk into a pitch-black chamber. Your fairy makes herself glow allowing you to see clearly.",
            descriptionMulti: "You walk into a pitch-black chamber. A party member's fairy makes herself glow allowing you to see clearly."
        },
        {
            item: "Lantern",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You walk into a pitch-black chamber. Lighting up your lantern, you navigate through easily.",
            descriptionMulti: "You walk into a pitch-black chamber. A party member lights up a lantern and you navigate through easily."
        },
        {
            item: "Glowing Crystal",
            consumed: true,
            quantity: 1,
            descriptionSolo: "You walk into a pitch-black chamber. You throw a Glowing Crystal on the ground, which shatters into small pieces. The glowing fragments light up the floor and you navigate through with ease.",
            descriptionMulti: "You walk into a pitch-black chamber. A party member throws a Glowing Crystal on the ground, which shatters into small pieces. The glowing fragments light up the floor and you navigate through with ease."
        },
        {
            item: "Spellbook",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You walk into a pitch-black chamber. You cast a spell from your spellbook so you can see in the dark.",
            descriptionMulti: "You walk into a pitch-black chamber. One of your party members casts a spell from a spellbook, allowing the party to see in the dark."
        },
        {
            item: "Magic Wand",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You walk into a pitch-black chamber. You create a flame from your magic wand to light up the path.",
            descriptionMulti: "You walk into a pitch-black chamber. One of your party members produces a flame from a magic wand, lighting up the path."
        },
        {
            item: "Good Luck Charm",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You walk into a pitch-black chamber. You stumble about in the darkness for a while, eventually finding your way out again.",
            descriptionMulti: "You walk into a pitch-black chamber. You stumble about in the darkness for a while, eventually finding your way out again."
        }
    ],
    toArea: null
}