exports.event = 
{
    descriptionSolo : "You find the path before you blocked with thick webbing. Unable to continue, you leave.",
    descriptionMulti : "You find the path before you blocked with thick webbing. Unable to continue, you leave.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: true,
    exceptions: 
    [
        {
            item: "Torch",
            consumed: true,
            quantity: 1,
            descriptionSolo: "You find the path before you blocked with thick spider webs. You take out a torch and burn a path through.",
            descriptionMulti: "You find the path before you blocked with thick spider webs. A party member takes out a torch and burns a path through."
        },
        {
            item: "Magic Wand",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You find the path before you blocked with thick spider webs. You take out your magic wand and burn a path through.",
            descriptionMulti: "You find the path before you blocked with thick spider webs. A party member takes out a magic wand and burns a path through."
        }
    ],
    toArea: null
}