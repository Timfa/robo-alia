exports.event = 
{
    descriptionSolo : "You encounter an underground lake. The water is clean and pure so you decide to wash yourself in it. Squeaky-clean, you continue.",
    descriptionMulti : "You encounter an underground lake. The water is clean and pure so you decide to wash yourself in it. Squeaky-clean, you continue.",

    loot:
    {
            name: "Fish",
            each: false,
            minQuantity: 1,
            maxQuantity: 10,
            conditional: true,
            conditionalRequirements:
            {
                items: ["Fishing Rod"],
                consumed: false,
                quantity: 1,
                descriptionSolo: "You encounter an underground lake. The water is clean and pure so you decide to wash yourself in it. Afterwards, you decide to try your luck and start fishing for a while.",
                descriptionMulti: "You encounter an underground lake. The water is clean and pure so the party decides to wash themselves in it. Meanwhile, one of your party members decides to try fishing."
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}