exports.event = 
{
    descriptionSolo : "As you move through the dungeon, you accidentally activate a trap! An arrow soars at you from a hole in the wall, wounding you badly! You limp home to recover.",
    descriptionMulti : "As the party moves through the dungeon, one of your party members is shot by an arrow trap! You quickly bring the wounded party member back home to recover.",

    loot:
    {
            name: "Arrow",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: true,
    exceptions: 
    [
        {
            item: "Shield",
            consumed: false,
            quantity: 1,
            descriptionSolo: "As you move through the dungeon, you accidentally activate a trap! An arrow soars at you from a hole in the wall, but you skillfully deflect it with your shield!",
            descriptionMulti: "As the party moves through the dungeon, one of your party members is shot by an arrow trap, but the arrow is skillfully deflected with a shield!"
        },
        {
            item: "Fairy",
            consumed: false,
            quantity: 1,
            descriptionSolo: "As you move through the dungeon, you accidentally activate a trap! An arrow soars at you from a hole in the wall, but your fairy companion casts a spell to block it!",
            descriptionMulti: "As the party moves through the dungeon, one of your party members is shot by an arrow trap, but a fairy casts a spell to block it!"
        },
        {
            item: "Identity Disk",
            consumed: false,
            quantity: 1,
            descriptionSolo: "As you move through the dungeon, you accidentally activate a trap! An arrow soars at you from a hole in the wall, but you skillfully deflect it with your disk!",
            descriptionMulti: "As the party moves through the dungeon, one of your party members is shot by an arrow trap, but the arrow is skillfully deflected with a disk!"
        },
        {
            item: "Good Luck Charm",
            consumed: false,
            quantity: 1,
            descriptionSolo: "As you move through the dungeon, you accidentally activate a trap! An arrow soars at you from a hole in the wall, but misses you!",
            descriptionMulti: "As the party moves through the dungeon, one of your party members is shot by an arrow trap! Fortunately, the arrow seems to miss."
        }
    ],
    toArea: null
}