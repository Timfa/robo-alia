exports.event = 
{
    descriptionSolo : "You stumble upon a massive cavern. Dimly glowing crystals adorn most of the stone surface. You collect a few of them.",
    descriptionMulti : "You stumble upon a massive cavern. Dimly glowing crystals adorn most of the stone surface. Your party collects a few of them.",

    loot:
    {
            name: "Glowing Crystal",
            each: true,
            minQuantity: 4,
            maxQuantity: 7,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}