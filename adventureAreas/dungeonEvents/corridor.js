exports.event = 
{
    descriptionSolo : "You make your way though an ordinary corridor.",
    descriptionMulti : "You make your way though an ordinary corridor.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Berry",
            consumed: true,
            quantity: 5,
            descriptionSolo: "You make your way though an ordinary corridor while snacking on some berries.",
            descriptionMulti: "You make your way though an ordinary corridor. A party member is snacking on berries."
        }
    ],
    toArea: null
}