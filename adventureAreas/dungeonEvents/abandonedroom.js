exports.event =
{
    descriptionSolo : "You enter an abandoned room.\nInside, you find a gemstone!",
    descriptionMulti : "You enter an abandoned room.\nInside, you find a single gemstone!",

    loot:
    {
            name: "Fancy Gemstone",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}