exports.event = 
{
    descriptionSolo : "You find a large chest. Unfortunately it's locked, so you leave it.",
    descriptionMulti : "You find a large chest. Unfortunately it's locked, so you leave it.",

    loot:
    {
        name: "Coin",
        each: true,
        minQuantity: 50,
        maxQuantity: 150,
        conditional: true,
        conditionalRequirements:
        {
            items: ["Lockpicking Set"],
            consumed: false,
            quantity: 1,
            descriptionSolo: "You find a large chest. You pick the lock and find a pile of coins inside!",
            descriptionMulti: "You find a large chest. A party member picks the lock and finds a pile of coins inside!"
        }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [ ],
    toArea: null
}