exports.event = 
{
    descriptionSolo : "You encounter a friendly old {manwoman} who tells you it's important to eat healthy food. {HeShe} gives you a handful of berries and leaves.",
    descriptionMulti : "You encounter a friendly old {manwoman} who tells you it's important to eat healthy food. {HeShe} gives each party member a handful of berries and leaves.",

    loot:
    {
            name: "Berry",
            each: true,
            minQuantity: 8,
            maxQuantity: 15,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}