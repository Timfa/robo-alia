exports.event = 
{
    descriptionSolo : "You encounter a wizard! {HeShe} casts a spell on you, instantaneously teleporting you back to the dungeon entrance!",
    descriptionMulti : "You encounter a wizard! {HeShe} casts a spell on one of your party members, who instantaneously disappears in a puff of smoke! The wizard quickly runs away.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: true,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Warding Amulet",
            consumed: true,
            quantity: 1,
            descriptionSolo: "You encounter a wizard! {HeShe} casts a spell on you, but your Warding Amulet protects you! Disgruntled, the wizard quickly teleports {himher}self away.",
            descriptionMulti: "You encounter a wizard! {HeShe} casts a spell on one of your party members, who is protected by a Warding Amulet! Disgruntled, the wizard quickly teleports {himher}self away."
        },
        {
            item: "Spellbook",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You encounter a wizard! {HeShe} casts a spell on you, but you counter it with one of your own! The wizard is teleported away.",
            descriptionMulti: "You encounter a wizard! {HeShe} casts a spell on you, but a party member counters it! The wizard is teleported away."
        }
    ],
    toArea: null
}