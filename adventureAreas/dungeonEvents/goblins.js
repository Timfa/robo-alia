exports.event = 
{
    descriptionSolo : "You encounter a group of goblins. You manage to carefully evade their detection and continue.",
    descriptionMulti : "You encounter a group of goblins. You manage to carefully evade their detection and continue.",

    loot:
    {
            name: "Gold Ring",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: true,
            conditionalRequirements:
            {
                items: ["Shortsword", "Elven Sword", "Dwarven Sword", "Magic Wand", "Spellbook", "Identity Disk"],
                consumed: false,
                quantity: 1,
                descriptionSolo: "You encounter a goblin. After defeating it, you notice a gold ring it dropped.",
                descriptionMulti: "You encounter a group of goblins. After defeating them, one of your party members notices one of the goblins had a gold ring!"
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}