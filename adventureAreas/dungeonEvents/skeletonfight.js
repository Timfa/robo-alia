exports.event = 
{
    descriptionSolo : "You pass though a dark chamber and stumble upon a skeleton!\nIt chases you out and you end your adventure.",
    descriptionMulti : "You pass through a dark chamber but stumble upon a skeleton!\nOne of your party members attracts its attention, allowing the rest to continue.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: true,
    forceEnd: false,
    exceptions: [
        {
            item: "Shortsword",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You pass though a dark chamber and stumble upon a skeleton!\nYou use your sword to fight and defeat it.",
            descriptionMulti: "You pass through a dark chamber but stumble upon a skeleton!\nOne of your party members grabs a sword, fights it and eventually defeats it."
        },
        {
            item: "Identity Disk",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You pass though a dark chamber and stumble upon a skeleton!\nWith a swift motion you throw your disk right through it, shattering it to pieces.",
            descriptionMulti: "You pass through a dark chamber but stumble upon a skeleton!\nOne of your party members grabs a disk and throws it clean trough the skeleton, shattering it to pieces."
        },
        {
            item: "Elven Sword",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You pass though a dark chamber and stumble upon a skeleton!\nYou use your sword to fight and defeat it.",
            descriptionMulti: "You pass through a dark chamber but stumble upon a skeleton!\nOne of your party members grabs a sword, fights it and eventually defeats it."
        },
        {
            item: "Dwarven Sword",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You pass though a dark chamber and stumble upon a skeleton!\nYou use your sword to fight and defeat it.",
            descriptionMulti: "You pass through a dark chamber but stumble upon a skeleton!\nOne of your party members grabs a sword, fights it and eventually defeats it."
        },
        {
            item: "Spellbook",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You pass though a dark chamber and stumble upon a skeleton!\nYou cast a spell from your spellbook, turning it into dust!",
            descriptionMulti: "You pass through a dark chamber but stumble upon a skeleton!\nOne of your party members casts a spell from a spellbook, turning it into dust!"
        },
        {
            item: "Fairy",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You pass though a dark chamber and stumble upon a skeleton!\nYour fairy companion casts a spell and the skeleton falls apart.",
            descriptionMulti: "You pass through a dark chamber but stumble upon a skeleton!\nOne of your party members' fairy companion casts a spell and the skeleton falls apart."
        },
        {
            item: "Magic Wand",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You pass though a dark chamber and stumble upon a skeleton!\nYou use your magic wand to blow it to pieces!",
            descriptionMulti: "You pass through a dark chamber but stumble upon a skeleton!\nOne of your party members uses a magic wand to blow it to pieces!"
        }
    ],
    toArea: null
}