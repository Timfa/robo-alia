exports.event = 
{
    descriptionSolo : "You hear a loud sound up ahead. As you move to investigate, you find the dungeon has collapsed and your path forward is blocked. You return, being unable to continue.",
    descriptionMulti : "You hear a loud sound up ahead. As you move to investigate, you find the dungeon has collapsed and your path forward is blocked. You return, being unable to continue.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: true,

    exceptions: 
    [
        {
            item: "Pickaxe",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You hear a loud sound up ahead. As you move to investigate, you find the dungeon has collapsed and your path forward is blocked. You grab your Pickaxe and dig your way through.",
            descriptionMulti: "You hear a loud sound up ahead. As you move to investigate, you find the dungeon has collapsed and your path forward is blocked. One of your party members takes out a Pickaxe and digs a path."
        },
        {
            item: "Shovel",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You hear a loud sound up ahead. As you move to investigate, you find the dungeon has collapsed and your path forward is blocked. You grab your Shovel and dig your way through.",
            descriptionMulti: "You hear a loud sound up ahead. As you move to investigate, you find the dungeon has collapsed and your path forward is blocked. One of your party members takes out a Shovel and digs a path."
        },
        {
            item: "Magic Wand",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You hear a loud sound up ahead. As you move to investigate, you find the dungeon has collapsed and your path forward is blocked. You take out your wand and blow the passage open.",
            descriptionMulti: "You hear a loud sound up ahead. As you move to investigate, you find the dungeon has collapsed and your path forward is blocked. A party member takes out a wand and blows the passage open."
        },
        {
            item: "Good Luck Charm",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You hear a loud sound up ahead. As you move to investigate, you find the dungeon has collapsed! Fortunately, you notice a door next to it that will lead you around the rubble.",
            descriptionMulti: "You hear a loud sound up ahead. As you move to investigate, you find the dungeon has collapsed. Fortunately, a party member notices a door next to it that will lead you around the rubble."
        }
    ],
    toArea: null
}