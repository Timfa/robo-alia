exports.event = 
{
    descriptionSolo : "You enter what looks like a small shrine to an ancient god. There are no other doors and you assume this is a dead end. You turn around and head back home.",
    descriptionMulti : "You enter what looks like a small shrine to an ancient god. There are no other doors and you assume this is a dead end. You turn around and head back home.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: true,
    exceptions:  
    [
        {
            item: "Coin",
            consumed: true,
            quantity: 10,
            descriptionSolo: "You enter what looks like a small shrine to an ancient god. You notice a bowl and make a small offering of coins. As you do, a hidden passage opens and allows you to pass through.",
            descriptionMulti: "You enter what looks like a small shrine to an ancient god. One of your party members notices a bowl and makes a small offering of coins. After this, a hidden passage opens allowing you to pass through."
        }
    ],
    toArea: null
}