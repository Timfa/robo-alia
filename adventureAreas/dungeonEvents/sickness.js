exports.event = 
{
    descriptionSolo : "As you walk though a corridor, you become very sick. You decide it's best to return until you recover.",
    descriptionMulti : "As you walk though a corridor, a party member becomes very sick and decides it's best to return.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: true,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Medicine",
            consumed: true,
            quantity: 1,
            descriptionSolo: "As you walk though a corridor, you become very sick. You take some medicine, which makes you feel better.",
            descriptionMulti: "As you walk though a corridor, a party member becomes very sick. Fortunately the party has some medicine, which makes the party member feel better again."
        },
        {
            item: "Fairy",
            consumed: false,
            quantity: 1,
            descriptionSolo: "As you walk though a corridor, you become very sick. Your fairy companion casts a spell on you, which makes you feel better.",
            descriptionMulti: "As you walk though a corridor, a party member becomes very sick. Fortunately a fairy manages to cast a spell which makes the party member feel better again."
        }
    ],
    toArea: null
}