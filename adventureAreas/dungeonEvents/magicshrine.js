exports.event = 
{
    descriptionSolo : "You encounter a small crude shrine, which has mostly broken apart over time. There is a magic wand in the rubble.",
    descriptionMulti : "You encounter a small crude shrine, which has mostly broken apart over time. There is a magic wand in the rubble.",

    loot:
    {
        name: "Magic Wand",
        each: false,
        minQuantity: 1,
        maxQuantity: 1,
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 5,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [ ],
    toArea: null
}