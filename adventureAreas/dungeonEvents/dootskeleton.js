exports.event =
{
    descriptionSolo : "You enter a room in which you encounter a spooky skeleton. It doot doots on its trumpet and won’t let you pass.",
    descriptionMulti : "You enter a room in which you encounter a spooky skeleton. It doot doots on its trumpet and won’t let you pass.",

    loot:
    {
        name: "nothing",
        each: true,
        minQuantity: 0,
        maxQuantity: 0,
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 1,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    },

    killMember: false,
    forceEnd: true,
    exceptions: 
    [
        {
            item: "Pair of Dancing Shoes",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You enter a room in which you encounter a spooky skeleton. It doot doots on its trumpet and won’t let you pass. However, you put on a pair of Dancing Shoes and start dancing to its tunes. Impressed with your moves, the skeleton allows you to pass.",
            descriptionMulti: "You enter a room in which you encounter a spooky skeleton. It doot doots on its trumpet and won’t let you pass. However, one of your party members puts on a pair of Dancing Shoes and start dancing to the skeleton's tunes. Impressed with your party member's moves, the skeleton allows you to pass."
        }
    ],
    toArea: null
}