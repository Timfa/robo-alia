exports.event = 
{
    descriptionSolo : "You meet a group of adventurers. You decide to set up camp together and eat lunch while sharing stories.",
    descriptionMulti : "You meet another group of adventurers. You decide to set up camp together and eat lunch while sharing stories.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Berry",
            consumed: true,
            quantity: 30,
            descriptionSolo: "You meet a group of adventurers. You decide to set up camp together and eat lunch while sharing stories. You share some berries with the adventurers.",
            descriptionMulti: "You meet another group of adventurers. You decide to set up camp together and eat lunch while sharing stories. One of your party members decides to share some berries."
        }
    ],
    toArea: null
}