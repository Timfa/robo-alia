exports.event = 
{
    descriptionSolo : "You encounter a giant spider and wisely run for your life.",
    descriptionMulti : "You encounter a giant spider and wisely run for your life.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: true,
    exceptions: 
    [
        {
            item: "Shortsword",
            consumed: true,
            quantity: 1,
            descriptionSolo: "You encounter a giant spider. You take out your sword and fight it. You deal a mortal blow which breaks your sword. Victorious, you continue.",
            descriptionMulti: "You encounter a giant spider. One of your party members takes out a sword and fights it. The party member deals a mortal blow which breaks the sword. Victorious, you continue."
        },
        {
            item: "Elven Sword",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You encounter a giant spider. You take out your sword and defeat it. Victorious, you continue.",
            descriptionMulti: "You encounter a giant spider. One of your party members takes out a sword and defeats it. Victorious, you continue."
        },
        {
            item: "Identity Disk",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You encounter a giant spider. You take out your disk and cut off its legs. Victorious, you continue.",
            descriptionMulti: "You encounter a giant spider. One of your party members takes out a disk and cuts off its legs. Victorious, you continue."
        },
        {
            item: "Dwarven Sword",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You encounter a giant spider. You take out your sword and defeat it. Victorious, you continue.",
            descriptionMulti: "You encounter a giant spider. One of your party members takes out a sword and defeats it. Victorious, you continue."
        },
        {
            item: "Magic Wand",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You encounter a giant spider. You take out your magic wand and turn it into a thin mist.",
            descriptionMulti: "You encounter a giant spider. A party member takes out a wand and turns it into a thin mist."
        },
        {
            item: "Fairy",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You encounter a giant spider. Your fairy companion casts a spell, shrinking the spider to a manageable size.",
            descriptionMulti: "You encounter a giant spider. A party member's fairy companion casts a spell, shrinking the spider to a manageable size."
        }
    ],
    toArea: null
}