exports.event = 
{
    descriptionSolo : "You accidentally enter the lair of an ancient dragon. Since you're alone, you immediately run and leave the dungeon.",
    descriptionMulti : "You accidentally enter the lair of an ancient dragon. As you prepare for a fight, the dragon agrees to let you all pass if one person stays behind to help with an errand.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: true,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Glowing Crystal",
            consumed: true,
            quantity: 1,
            descriptionSolo: "You accidentally enter the lair of an ancient dragon. It notices you and blocks the exits. You offer it a Glowing Crystal and it decides to let you pass.",
            descriptionMulti: "You accidentally enter the lair of an ancient dragon. As you prepare for a fight, one of your party members steps forward confidently, offering the dragon a Glowing Crystal if it agrees to let you pass, which it does. You quickly continue."
        },
        {
            item: "Good Luck Charm",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You accidentally enter the lair of an ancient dragon. It seems to be asleep and doesn't notice you.",
            descriptionMulti: "You accidentally enter the lair of an ancient dragon. It seems to be asleep and doesn't notice the party."
        }
    ],
    toArea: null
}