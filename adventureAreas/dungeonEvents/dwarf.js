exports.event = 
{
    descriptionSolo : "You encounter the body of a Dwarf, who appears to have died fighting. In {hisher} hand {heshe} clutches a Dwarven Sword, which you immediately loot.",
    descriptionMulti : "You encounter the body of a Dwarf, who appears to have died fighting. In {hisher} hand {heshe} clutches a Dwarven Sword, which one of your party members immediately loots.",

    loot:
    {
            name: "Dwarven Sword",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}