exports.event = 
{
    descriptionSolo : "You encounter a succubus! She seduces you and you black out. You wake up outside the dungeon, feeling a bit funny. You decide to go home.",
    descriptionMulti : "You encounter a succubus! The party escapes the room, but when they stop running they notice one member failed to resist seduction.",

    loot:
    {
        name: "nothing",
        each: true,
        minQuantity: 0,
        maxQuantity: 0,
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 1,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    },

    killMember: true,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Warding Amulet",
            consumed: true,
            quantity: 1,
            descriptionSolo: "You encounter a succubus! She tries to seduce you, but your warding amulet protects you. You quickly escape.",
            descriptionMulti: "You encounter a succubus! The party escapes the room. One party member notices that a Warding Amulet had protected the party from seduction."
        }
    ],
    toArea: null
}