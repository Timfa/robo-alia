exports.event = 
{
    descriptionSolo : "You meet a very sick adventurer. Unable to help, you wish {himher} luck and continue.",
    descriptionMulti : "You meet a very sick adventurer. Unable to help, you wish {himher} luck and continue.",

    loot:
    {
        name: "Gold Necklace",
        each: false,
        minQuantity: 1,
        maxQuantity: 1,
        conditional: true,
        conditionalRequirements:
        {
            items: ["Medicine"],
            consumed: true,
            quantity: 1,
            descriptionSolo: "You meet a very sick adventurer. You give {himher} some of your medicine and {heshe} soon feels much better. As thanks, {heshe} rewards you with a necklace.",
            descriptionMulti: "You meet a very sick adventurer. A party member gives {himher} some medicine and {heshe} soon feels much better. {HeShe} offers a necklace as thanks."
        }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [ ],
    toArea: null
}