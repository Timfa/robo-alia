exports.event = 
{
    descriptionSolo : "You encounter an underground settlement inhabited by small grey-skinned creatures. They are friendly, so you have a look around before you continue.",
    descriptionMulti : "You encounter an underground settlement inhabited by small grey-skinned creatures. They are friendly, so you have a look around before you continue.",

    loot:
    {
            name: "Good Luck Charm",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: true,
            conditionalRequirements:
            {
                items: ["Coin"],
                consumed: true,
                quantity: 5,
                descriptionSolo: "You encounter an underground settlement inhabited by small grey-skinned creatures. They are friendly, so you have a look around before you continue. You buy what appears to be a charm of some sort from a local shop as a souvenir.",
                descriptionMulti: "You encounter an underground settlement inhabited by small grey-skinned creatures. They are friendly, so you have a look around before you continue. A party member buys what appears to be a charm of some sort from a local shop as a souvenir."
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "none",
            consumed: false,
            quantity: 1,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    ],
    toArea: null
}