exports.event = 
{
    descriptionSolo : "You walk through a long corridor and encounter a duck. You walk over and pick it up.\nShould have known better than to do that...",
    descriptionMulti : "You walk through a long corridor and encounter a duck. One of your party members walks over and picks it up.\nShould have known better than to do that...",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: true,
    forceEnd: false,
    exceptions: [],
    toArea: null
}