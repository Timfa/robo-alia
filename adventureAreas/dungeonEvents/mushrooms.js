exports.event = 
{
    descriptionSolo : "You enter a large open space. Vegetation fills the room and you see a large amount of mushrooms growing on the walls. You pick a few of them and continue.",
    descriptionMulti : "Your party enters a large open space. Vegetation can be found all over the walls. You pick some of the mushrooms growing there and share them with your party.",

    loot:
    {
            name: "Mushroom",
            each: true,
            minQuantity: 3,
            maxQuantity: 12,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}