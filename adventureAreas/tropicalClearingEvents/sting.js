exports.event = 
{
    descriptionSolo : "As you walk through an overgrown area, you suddenly feel a sharp pain in your leg. Looking closely, you see you've been stung by a small yellow bug. You start to become dizzy and eventually pass out.",
    descriptionMulti : "As you walk through an overgrown area, a party member screams out in pain! As you hurry over to investigate, you can clearly see an insect sting. Your party member eventually loses conciousness, so you quickly get back to safety.",

    loot:
    {
        name: "nothing",
        each: false,
        minQuantity: 100,
        maxQuantity: 500,
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 5,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    },

    killMember: false,
    forceEnd: true,
    exceptions: 
    [
        {
            item: "Fairy",
            consumed: false,
            quantity: 1,
            descriptionSolo: "As you walk through an overgrown area, you suddenly feel a sharp pain in your leg. Looking closely, you see you've been stung by a small yellow bug. Your fairy companion is familiar with them and knows a spell that reduces the swelling, until it eventually disappears entirely.",
            descriptionMulti: "As you walk through an overgrown area, a party member screams out in pain! As you hurry over to investigate, you can clearly see an insect sting. Fortunately, a fairy companion is familiar with them and knows a spell that reduces the swelling, until it eventually disappears entirely."
        }
    ],
    toArea: null
}