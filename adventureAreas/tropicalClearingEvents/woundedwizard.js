exports.event = 
{
    descriptionSolo : "You encounter a badly wounded wizard. As you try to help, {heshe} shakes {hisher} head and gives you {hisher} spellbook, after which {heshe} breathes {hisher} last.",
    descriptionMulti : "You encounter a badly wounded wizard. As you try to help, {heshe} shakes {hisher} head and gives you {hisher} spellbook, after which {heshe} breathes {hisher} last.",

    loot:
    {
        name: "Spellbook",
        each: false,
        minQuantity: 1,
        maxQuantity: 1,
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 5,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [ ],
    toArea: null
}