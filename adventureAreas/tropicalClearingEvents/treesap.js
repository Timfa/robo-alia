exports.event = 
{
    descriptionSolo : "You find a tree oozing a clear yellow sap. You take some with you.",
    descriptionMulti : "You find a tree oozing a clear yellow sap, which one of your party members collects.",

    loot:
    {
        name: "Vial of Tree Sap",
        each: false,
        minQuantity: 1,
        maxQuantity: 3,
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 5,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [ ],
    toArea: null
}