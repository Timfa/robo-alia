exports.event = 
{
    descriptionSolo : "You encounter a small fairy sitting under a tree. You pick her up and take her with you.",
    descriptionMulti : "You encounter a small fairy sitting under a tree. A party member picks her up and takes her along on the adventure.",

    loot:
    {
        name: "Fairy",
        each: false,
        minQuantity: 1,
        maxQuantity: 1,
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 5,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [ ],
    toArea: null
}