exports.event = 
{
    descriptionSolo : "A giant bird swoops down, picks you up and carries you away! It drops you into a nest, where you wait until nightfall to escape. As it's very late now, you decide to go home.",
    descriptionMulti : "A giant bird swoops down, picks up a party member and flies away!",

    loot:
    {
        name: "nothing",
        each: false,
        minQuantity: 1,
        maxQuantity: 3,
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 5,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    },

    killMember: true,
    forceEnd: false,
    exceptions: 
    [ 
        {
            item: "Good Luck Charm",
            consumed: true,
            quantity: 1,
            descriptionSolo: "A giant bird swoops down and tries to snatch you away, but it grabs onto a lucky charm's cord, saving you but ruining the charm.",
            descriptionMulti: "A giant bird swoops down and tries to snatch a party member away, but it grabs onto a lucky charm's cord, saving the party member but ruining the charm."
        }
    ],
    toArea: null
}