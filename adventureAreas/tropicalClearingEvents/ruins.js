exports.event = 
{
    descriptionSolo : "You encounter a very old ruin. Looking through the rubble, you find a chest full of treasure!",
    descriptionMulti : "You encounter a very old ruin. Looking through the rubble, one of your party members finds a chest full of treasure!",

    loot:
    {
        name: "Coin",
        each: true,
        minQuantity: 100,
        maxQuantity: 500,
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 5,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [ ],
    toArea: null
}