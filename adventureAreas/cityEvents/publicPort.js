exports.event = 
{
    descriptionSolo : "You arrive at the city port. You chat with one of the sailors for a while.",
    descriptionMulti : "You arrive at the city port. A party member chats with one of the sailors for a moment, while another watches the ships come and go.",

    loot:
    {
        name: "nothing",
        each: false,
        minQuantity: 1,
        maxQuantity: 1,
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 5,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [ ],
    toArea: 
    { 
        areaName: "Ocean",
        conditional: true,
        conditionalRequirements:
        {
            items: ["Boarding Pass"],
            consumed: true,
            quantity: 1,
            descriptionSolo: "You arrive at the city port. You stroll up to a ship looking for passengers and present your Boarding Pass.",
            descriptionMulti: "You arrive at the city port. You stroll up to a ship looking for passengers and present your Boarding Pass."
        }
    }
}