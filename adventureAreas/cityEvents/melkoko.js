exports.event = 
{
    descriptionSolo : "You enter the kitchen of a hospitable cook introducing {himher}self as {name}. {HeShe} feeds you some mushroom soup. After eating the soup you feel like you're too full to continue your adventure and decide to go home. {name} packs some more soup for you.",
    descriptionMulti : "You enter the kitchen of a hospitable cook introducing {himher}self as {name}. {HeShe} feeds the entire party some mushroom soup. Everyone is too full to continue the adventure, so you decide to go home. {name} packs everyone some more soup for later.",

    loot:
    {
            name: "Mushroom Soup",
            each: true,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: true,
    exceptions: [],
    toArea: null
}