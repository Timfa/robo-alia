exports.event = 
{
    descriptionSolo : "You see something shiny on top of a high wall! You try to climb it, but only end up falling down and hurting yourself.",
    descriptionMulti : "A party member sees something shiny on top of a high wall! The party member can't seem to manage climbing up the wall though, as several loose stones fall down.",

    loot:
    {
            name: "Coin",
            each: true,
            minQuantity: 9,
            maxQuantity: 30,
            conditional: true,
            conditionalRequirements:
            {
                items: ["Climbing Gear"],
                consumed: false,
                quantity: 1,
                descriptionSolo: "You see something shiny on top of a high wall! You scale it, finding a secret stash of coins!",
                descriptionMulti: "A party member sees something shiny on top of a high wall! Scaling it with some equipment, the party finds a hidden stash of coins!"
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [
        
    ],
    toArea: null
}