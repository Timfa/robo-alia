exports.event = 
{
    descriptionSolo : "You encounter a cat cafe. You have no money to pay the entrance fee, so you don't go inside.",
    descriptionMulti : "You encounter a cat cafe. Nobody has any money to pay the entrance fee, so you don't go inside.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 5,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Coin",
            consumed: true,
            quantity: 5,
            descriptionSolo: "You encounter a cat cafe. You head inside and play with the kitties for a while.",
            descriptionMulti: "You encounter a cat cafe. You head inside and play with the kitties for a while."
        }
    ],
    toArea: null
}