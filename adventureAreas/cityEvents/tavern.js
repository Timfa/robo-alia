exports.event = 
{
    descriptionSolo : "You encounter a tavern. You have no money, so you don't go inside.",
    descriptionMulti : "You encounter a tavern. Nobody has any money, so you don't go inside.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 5,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Coin",
            consumed: true,
            quantity: 3,
            descriptionSolo: "You encounter a tavern. You head inside, talk to the locals and have a drink.",
            descriptionMulti: "You encounter a tavern. You head inside, talk to the locals and have a drink."
        }
    ],
    toArea: null
}