exports.event = 
{
    descriptionSolo : "You meet an Elven blacksmith called {name}. {HeShe} agrees to make you a sword if you help {himher} out in {hisher} workshop. You work with {himher} for the rest of the day, after which you head home.",
    descriptionMulti : "You meet an Elven blacksmith called {name}. {HeShe} agrees to make one of you a sword if you help {himher} out in {hisher} workshop. You work with {himher} for the rest of the day, after which you head home.",

    loot:
    {
            name: "Elven Sword",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: true,
    exceptions: [],
    toArea: null
}