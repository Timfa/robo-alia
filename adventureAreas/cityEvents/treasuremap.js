exports.event = 
{
    descriptionSolo : "You encounter an old {manwoman}. Eyeing you suspiciously, {heshe} approaches you and tells you of a great buried treasure. {HeShe} isn't fit enough to seek it out {himher}self anymore, so {heshe} decides to give you the map.",
    descriptionMulti : "You encounter an old {manwoman}. Eyeing you suspiciously, {heshe} approaches you and tells you of a great buried treasure. {HeShe} isn't fit enough to seek it out {himher}self anymore, so {heshe} decides to give the map to one of your party members.",

    loot:
    {
            name: "Treasure Map",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 5,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}