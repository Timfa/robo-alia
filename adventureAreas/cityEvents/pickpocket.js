exports.event = 
{
    descriptionSolo : "A {manwoman} bumps into you. Looking behind you, the {manwoman} already seems to have disappeared into the crowd.",
    descriptionMulti : "A {manwoman} bumps into a party member. Looking around, the {manwoman} already seems to have disappeared into the crowd.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 5,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Coin",
            consumed: true,
            involuntary: true,
            quantity: 10,
            descriptionSolo: "A {manwoman} bumps into you. Looking behind you, the {manwoman} already seems to have disappeared into the crowd. You notice a few coins missing.",
            descriptionMulti: "A {manwoman} bumps into a party member. Looking around, the {manwoman} already seems to have disappeared into the crowd. The party member notices some coins missing."
        },
        {
            item: "Fairy",
            consumed: false,
            quantity: 1,
            descriptionSolo: "A {manwoman} bumps into you. Looking behind you, the {manwoman} already seems to have disappeared into the crowd. You notice your fairy companion furiously guarding your coin pouch.",
            descriptionMulti: "A {manwoman} bumps into a party member. Looking around, the {manwoman} already seems to have disappeared into the crowd. The party member notices a fairy companion furiously guarding a coin pouch."
        },
        {
            item: "Good Luck Charm",
            consumed: false,
            quantity: 1,
            descriptionSolo: "A {manwoman} bumps into you. Looking behind you, the {manwoman} already seems to have disappeared into the crowd. You notice the {manwoman} had apparently tried to pickpocket you, but {heshe} failed to do so successfully.",
            descriptionMulti: "A {manwoman} bumps into a party member. Looking around, the {manwoman} already seems to have disappeared into the crowd. The party member notices the {manwoman} had apparently tried to steal something, but {heshe} failed to do so successfully."
        }
    ],
    toArea: null
}