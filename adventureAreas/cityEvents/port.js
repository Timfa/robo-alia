exports.event = 
{
    descriptionSolo : "You arrive at the city port. You watch the ships come and go for a while before moving on.",
    descriptionMulti : "You arrive at the city port. A party member watches the ships come and go for a moment, while another chats with one of the sailors.",

    loot:
    {
        name: "nothing",
        each: false,
        minQuantity: 1,
        maxQuantity: 1,
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 5,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [ ],
    toArea: 
    { 
        areaName: "Ocean",
        conditional: true,
        conditionalRequirements:
        {
            items: ["Ship"],
            consumed: false,
            quantity: 1,
            descriptionSolo: "You arrive at the city port. You stroll up to your ship and set off.",
            descriptionMulti: "You arrive at the city port. One of your party members mentions owning a ship and suggests using it. You climb aboard and set off."
        }
    }
}