exports.event = 
{
    descriptionSolo : "You walk past a house of an old {manwoman}. {HeShe} asks you to help {himher} rake some leaves, as {heshe} is too old to do it {himher}self. {HeShe} gives you some coins as a reward.",
    descriptionMulti : "You walk past a house of an old {manwoman}. {HeShe} asks you to help {himher} rake some leaves, as {heshe} is too old to do it {himher}self. {HeShe} gives you some coins as a reward.",

    loot:
    {
            name: "Coin",
            each: true,
            minQuantity: 3,
            maxQuantity: 7,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}