exports.event = 
{
    descriptionSolo : "You find a wizard called {name} who appears to be setting up an expedition. {HeShe} asks for your help, offering you money to explore a hidden area. You accept and {name} casts a teleportation spell on you.",
    descriptionMulti : "You find a wizard called {name} who appears to be setting up an expedition. {HeShe} asks for your help, offering you money to explore a hidden area. You accept and {name} casts a teleportation spell on your party.",

    loot:
    {
            name: "Coin",
            each: true,
            minQuantity: 30,
            maxQuantity: 50,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 5,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "none",
            consumed: false,
            quantity: 1,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    ],
    toArea: 
    { 
        areaName: "Tropical Clearing",
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 5,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    }
}