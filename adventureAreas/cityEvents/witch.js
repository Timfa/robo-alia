exports.event = 
{
    descriptionSolo : "As you stroll through a nearby alley an elderly woman approaches. It turns out she is a witch and turns you into a frog!",
    descriptionMulti : "As you stroll through a nearby alley an elderly woman approaches. It turns out she is a witch and turns one of your party members into a frog! The witch quickly runs away.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: true,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Warding Amulet",
            consumed: true,
            quantity: 1,
            descriptionSolo: "As you stroll through a nearby alley an elderly woman approaches. It turns out she is a witch and casts a spell on you, but your Warding Amulet protects you! Disgruntled, the witch quickly teleports herself away.",
            descriptionMulti: "As you stroll through a nearby alley an elderly woman approaches. It turns out she is a witch and casts a spell on one of your party members, who is protected by a Warding Amulet! Disgruntled, the witch quickly teleports herself away."
        },
        {
            item: "Fairy",
            consumed: false,
            quantity: 1,
            descriptionSolo: "As you stroll through a nearby alley an elderly woman approaches. It turns out she is a witch and casts a spell on you, but your fairy companion protects you! Disgruntled, the witch quickly teleports herself away.",
            descriptionMulti: "As you stroll through a nearby alley an elderly woman approaches. It turns out she is a witch and casts a spell on one of your party members, who is protected by a fairy! Disgruntled, the witch quickly teleports herself away."
        },
        {
            item: "Spellbook",
            consumed: false,
            quantity: 1,
            descriptionSolo: "As you stroll through a nearby alley an elderly woman approaches. It turns out she is a witch and casts a spell on you, but you counter it with one of your own! The witch is teleported away.",
            descriptionMulti: "As you stroll through a nearby alley an elderly woman approaches. It turns out she is a witch and casts a spell on you, but a party member counters it! The witch is teleported away."
        }
    ],
    toArea: null
}