exports.event = 
{
    descriptionSolo : "You find some coins on the street!",
    descriptionMulti : "You find some coins on the street!",

    loot:
    {
        name: "Coin",
        each: false,
        minQuantity: 3,
        maxQuantity: 7,
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 1,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}