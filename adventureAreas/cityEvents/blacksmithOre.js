exports.event = 
{
    descriptionSolo : "You meet a blacksmith called {name}. You browse {hisher} shop for a while before moving on.",
    descriptionMulti : "You meet a blacksmith called {name}. You browse {hisher} shop for a while before moving on.",

    loot:
    {
            name: "Iron Ingot",
            each: false,
            minQuantity: 4,
            maxQuantity: 10,
            conditional: true,
            conditionalRequirements:
            {
                items: ["Iron Ore"],
                consumed: true,
                quantity: 10,
                descriptionSolo: "You meet a blacksmith called {name}. {HeShe} lets you use her workshop to process some of your ore.",
                descriptionMulti: "You meet a blacksmith called {name}. While the party browses the shop, {heshe} allows one of your party members to use her tools to process some ore."
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}