exports.area = 
{
    name: "Abandoned Laboratory",
    canStart: false,
    events:[]
}

var fs = require("fs");

fs.readdir("./adventureAreas/laboratoryEvents/", function(err, files)  {
    files.forEach(function(file) 
    {
        exports.area.events.push(require("./laboratoryEvents/" + file).event);
    });
})