exports.area =
{
    name: "Tropical Clearing",
    canStart: false,
    events:[]
}

var fs = require("fs");

fs.readdir("./adventureAreas/tropicalClearingEvents/", function(err, files) {
    files.forEach(function(file) 
    {
        exports.area.events.push(require("./tropicalClearingEvents/" + file).event);
    });
})