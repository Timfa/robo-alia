exports.area = 
{
    name: "Grid",
    canStart: false,
    events:[]
}

var fs = require("fs");

fs.readdir("./adventureAreas/gridEvents/", function(err, files) {
    files.forEach(function(file) 
    {
        exports.area.events.push(require("./gridEvents/" + file).event);
    });
})