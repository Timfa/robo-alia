exports.event = 
{
    descriptionSolo : "You approach an IO stream and watch the packets flow by for a while before moving on.",
    descriptionMulti : "You approach an IO stream and watch the packets flow by for a while before moving on.",

    loot:
    {
            name: "nothing",
            each: true,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}