exports.event = 
{
    descriptionSolo : "You meet a friendly program who offers you an Antiviral Subroutine.",
    descriptionMulti : "You meet a friendly program who offers your party some Antiviral Subroutines.",

    loot:
    {
            name: "Antiviral Subroutine",
            each: true,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}