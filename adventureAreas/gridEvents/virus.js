exports.event = 
{
    descriptionSolo : "Above you what seems like a large green electical storm appears. Suddenly it starts spitting out corrupted programs! You try to flee, but they manage to infect you. You are de-rezzed.",
    descriptionMulti : "Above you what seems like a large green electical storm appears. Suddenly it starts spitting out corrupted programs! You try to flee, but they manage to infect one of your party members, who is immediately overcome and de-rezzed.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["nothing"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: true,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Identity Disk",
            consumed: false,
            quantity: 1,
            descriptionSolo: "Above you what seems like a large green electical storm appears. Suddenly it starts spitting out corrupted programs! You manage to fight them off until antivirus programs appear and take care of the rest.",
            descriptionMulti: "Above you what seems like a large green electical storm appears. Suddenly it starts spitting out corrupted programs! A party member manages to fight them off until antivirus programs appear and take care of the rest."
        },
        {
            item: "Antiviral Subroutine",
            consumed: true,
            quantity: 1,
            descriptionSolo: "Above you what seems like a large green electical storm appears. Suddenly it starts spitting out corrupted programs! Thankfully your Antiviral Subroutine kicks in just in time and renders you immune.",
            descriptionMulti: "Above you what seems like a large green electical storm appears. Suddenly it starts spitting out corrupted programs! Thankfully your party member's Antiviral Subroutine kicks in just in time and renders the party immune."
        }
    ],
    toArea: null
}