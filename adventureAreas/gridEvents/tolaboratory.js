exports.event = 
{
    descriptionSolo : "You find a large glowing portal. A beam shoots out of it, transporting you back to the laboratory.",
    descriptionMulti : "You find a large glowing portal. A beam shoots out of it, transporting the party back to the laboratory.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 5,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [],
    toArea: 
    { 
        areaName: "Abandoned Laboratory",
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 5,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    }
}