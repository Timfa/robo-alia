exports.event = 
{
    descriptionSolo : "You move through a bustling hub where many programs go about their business.",
    descriptionMulti : "You move through a bustling hub where many programs go about their business.",

    loot:
    {
            name: "nothing",
            each: true,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}