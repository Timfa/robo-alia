exports.event = 
{
    descriptionSolo : "You come across four programs that beckon you over. As you come closer, they scan you and generate an Identity Disk for you.",
    descriptionMulti : "You come across four programs that beckon you over. As your party comes closer, they scan you and generate an Identity Disk for each you.",

    loot:
    {
            name: "Identity Disk",
            each: true,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}