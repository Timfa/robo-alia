exports.event = 
{
    descriptionSolo : "You cross a large bridge. In the distance you see a large shining tower.",
    descriptionMulti : "You cross a large bridge. In the distance you see a large shining tower.",

    loot:
    {
            name: "nothing",
            each: true,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}