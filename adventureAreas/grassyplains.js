exports.area = 
{
    name: "Grassy Plains",
    canStart: true,
    events:[]
}

var fs = require("fs");

fs.readdir("./adventureAreas/plainsEvents/", function(err, files)  {
    files.forEach(function(file) 
    {
        exports.area.events.push(require("./plainsEvents/" + file).event);
    });
})