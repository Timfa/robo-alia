exports.event = 
{
    descriptionSolo : "You get lost in a maze of narrow hallways. After several hours of aimless wandering you find a way out, having completely exhausted yourself. You decide to go home.",
    descriptionMulti : "Your group gets lost in a maze of narrow hallways. After several hours of aimless wandering you find a way out. You do a quick headcount at the exit, but notice someone is missing...",   

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 5,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: true,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Seashell",
            consumed: true,
            quantity: 40,
            descriptionSolo: "You get lost in a maze of narrow hallways. Marking your way with seashells you eventually determine the right way forwards.",
            descriptionMulti: "Your group gets lost in a maze of narrow hallways. Marking your way with seashells you eventually determine the right way forwards."
        },	  
        {
            item: "Spellbook",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You get lost in a maze of narrow hallways. Using your spellbook you cast a guiding orb of light and make your way out.",
            descriptionMulti: "Your group gets lost in a maze of narrow hallways. One of your party members uses a spellbook to cast a guiding orb of light showing you the way out."
        },
        {
            item: "Good Luck Charm",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You get lost in a maze of narrow hallways. You stumble about for a while and happen to find the exit by chance.",
            descriptionMulti: "Your group gets lost in a maze of narrow hallways. You stumble about for a while and happen to find the exit by chance."
        },
        
    ],
    toArea: null
}