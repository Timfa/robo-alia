exports.event = 
{
    descriptionSolo : "You come across a dark chamber with a desk and some strange machines. A display on the desk lights up, asking for a passkey. You don't have one.",
    descriptionMulti : "You come across a dark chamber with a desk and some strange machines. A display on the desk lights up, asking for a passkey. You don't have one.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 5,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "none",
            consumed: false,
            quantity: 1,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    ],
    toArea: 
    { 
        areaName: "Grid",
        conditional: true,
        conditionalRequirements:
        {
            items: ["Passkey"],
            consumed: false,
            quantity: 1,
            descriptionSolo: "You come across a dark chamber with a desk and some strange machines. A display on the desk lights up, asking for a passkey. Hesitantly, you hold your passkey out. A sound behind you startles you, but as you turn around you're shot by a blue beam! You wake up in an unfamiliar black landscape later. The sky is dark and there are brilliant blue streaking lights scattered across the surrounding space.",
            descriptionMulti: "You come across a dark chamber with a desk and some strange machines. A display on the desk lights up, asking for a passkey. Hesitantly, a party member produces a passkey. A sound behind you startles you, but as you turn around you're shot by a blue beam! You wake up in an unfamiliar black landscape later. The sky is dark and there are brilliant blue streaking lights scattered across the surrounding space."
        }
    }
}