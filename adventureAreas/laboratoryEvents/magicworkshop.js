exports.event = 
{
    descriptionSolo : "You find an abandoned desk with strange devices. A lone Magical Potion sits underneath it.",
    descriptionMulti : "You find an abandoned desk with strange devices. A lone Magical Potion sits underneath it.",

    loot:
    {
            name: "Magic Potion",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}