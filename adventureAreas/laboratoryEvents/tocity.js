exports.event = 
{
    descriptionSolo : "You decide to leave the laboratory.",
    descriptionMulti : "You decide to leave the laboratory.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 5,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "none",
            consumed: false,
            quantity: 1,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    ],
    toArea: 
    { 
        areaName: "City",
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 5,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    }
}