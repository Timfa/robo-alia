exports.area =
{
    name: "City",
    canStart: true,
    events:[]
}

var fs = require("fs");

fs.readdir("./adventureAreas/cityEvents/", function(err, files)  {
    files.forEach(function(file) 
    {
        exports.area.events.push(require("./cityEvents/" + file).event);
    });
})