exports.event = 
{
    descriptionSolo : "You encounter a trading vessel that inquires if you are selling fruit. You tell the captain you have none, and both go on your way.",
    descriptionMulti : "You encounter a trading vessel that inquires if you are selling fruit. You tell the captain you have none, and both go on your way.",

    loot:
    {
            name: "Coin",
            each: false,
            minQuantity: 250,
            maxQuantity: 500,
            conditional: true,
            conditionalRequirements:
            {
                items: ["Apple"],
                consumed: true,
                quantity: 50,
                descriptionSolo: "You encounter a trading vessel that inquires if you are selling fruit. You show him your wares and strike a profitable deal.",
                descriptionMulti: "You encounter a trading vessel that inquires if you are selling fruit. You show him your wares and strike a profitable deal."
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}