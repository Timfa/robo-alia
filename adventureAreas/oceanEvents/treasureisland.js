exports.event = 
{
    descriptionSolo : "You find an island. You explore it for a while, but find nothing of significance.",
    descriptionMulti : "You find an island. You explore it for a while, but find nothing of significance.",

    loot:
    {
            name: "Coin",
            each: true,
            minQuantity: 300,
            maxQuantity: 1000,
            conditional: true,
            conditionalRequirements:
            {
                items: ["Treasure Map"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "You find an island. Recognizing the rough shape, you pull out a Treasure Map. You follow it and stumble upon a large treasure chest!",
                descriptionMulti: "You find an island. Recognizing the rough shape, a party member pull out a Treasure Map. You follow it and stumble upon a large treasure chest!"
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [],
    toArea: null
}