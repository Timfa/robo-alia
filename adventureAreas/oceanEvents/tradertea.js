exports.event = 
{
    descriptionSolo : "You encounter a trading vessel that inquires if you are selling tea. You tell the captain you have none, and both go on your way.",
    descriptionMulti : "You encounter a trading vessel that inquires if you are selling tea. You tell the captain you have none, and both go on your way.",

    loot:
    {
            name: "Coin",
            each: false,
            minQuantity: 350,
            maxQuantity: 600,
            conditional: true,
            conditionalRequirements:
            {
                items: ["Tea Bag"],
                consumed: true,
                quantity: 100,
                descriptionSolo: "You encounter a trading vessel that inquires if you are selling tea. You show him your wares and strike a profitable deal.",
                descriptionMulti: "You encounter a trading vessel that inquires if you are selling tea. You show him your wares and strike a profitable deal."
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}