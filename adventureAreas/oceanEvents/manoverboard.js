exports.event = 
{
    descriptionSolo : "A large wave hits your ship, knocking you over. You hit the deck hard enough to lose conciousness. When you wake up, you notice you have drifted back to shore and that night has fallen.",
    descriptionMulti : "A large wave hits your ship, knocking a party member overboard.",

    loot:
    {
            name: "nothing",
            each: true,
            minQuantity: 300,
            maxQuantity: 1000,
            conditional: true,
            conditionalRequirements:
            {
                items: ["Treasure Map"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: true,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Rope",
            consumed: true,
            quantity: 1,
            descriptionSolo: "A large wave hits your ship, knocking you overboard! Fortunately, you have a rope with you and manage to climb back aboard.",
            descriptionMulti: "A large wave hits your ship, knocking a party member overboard! Fortunately, someone brought a rope and manages to help the party member climb back aboard."
        }
    ],
    toArea: null
}