exports.event = 
{
    descriptionSolo : "You sail through calm waters. You relax for a moment on deck, breathing in the salty air.",
    descriptionMulti : "You sail through calm waters. The party gathers on deck to relax.",

    loot:
    {
            name: "Fish",
            each: false,
            minQuantity: 10,
            maxQuantity: 30,
            conditional: true,
            conditionalRequirements:
            {
                items: ["Fishing Rod"],
                consumed: false,
                quantity: 1,
                descriptionSolo: "You sail through calm waters. Noticing an abundance of fish, you decide to try your hand at some fishing.",
                descriptionMulti: "You sail through calm waters. As the party relaxes on deck, one party member decides it is a good moment for some fishing."
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}