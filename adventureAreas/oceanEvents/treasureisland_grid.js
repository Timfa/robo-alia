exports.event = 
{
    descriptionSolo : "You find an island. You explore it for a while, but find nothing of significance.",
    descriptionMulti : "You find an island. You explore it for a while, but find nothing of significance.",

    loot:
    {
            name: "Passkey",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: true,
            conditionalRequirements:
            {
                items: ["Treasure Map"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "You find an island. Recognizing the rough shape, you pull out a Treasure Map. You follow it and stumble upon a large treasure chest! Inside is a black passkey, labeled \"DESTINY\".",
                descriptionMulti: "You find an island. Recognizing the rough shape, a party member pull out a Treasure Map. You follow it and stumble upon a large treasure chest! Inside is a black passkey, labeled \"DESTINY\"."
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [],
    toArea: null
}