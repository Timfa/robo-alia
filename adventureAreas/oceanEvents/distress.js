exports.event = 
{
    descriptionSolo : "You notice a ship in the distance flashing a distress signal at you. Sailing over to investigate. The ship's captain explains that their ship needs urgent repairs, but he has run out of equipment. As you have none yourself, you are unable to help.",
    descriptionMulti : "A party member notices a ship in the distance flashing a distress signal at your vessel. Sailing over to investigate. The ship's captain explains that their ship needs urgent repairs, but he has run out of equipment. As nobody in the party has any either, you are unable to help.",

    loot:
    {
            name: "Coin",
            each: true,
            minQuantity: 160,
            maxQuantity: 200,
            conditional: true,
            conditionalRequirements:
            {
                items: ["Repair Kit"],
                consumed: true,
                quantity: 1,
                descriptionSolo: "You notice a ship in the distance flashing a distress signal at you. Sailing over to investigate. The ship's captain explains that their ship needs urgent repairs, but he has run out of equipment. You climb aboard and help with repairs, after which the captain offers you some money as reward.",
                descriptionMulti: "A party member notices a ship in the distance flashing a distress signal at your vessel. Sailing over to investigate. The ship's captain explains that their ship needs urgent repairs, but he has run out of equipment. Your party climbs aboard to assist in repairs, after which the captain offers some money as reward."
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: [],
    toArea: null
}