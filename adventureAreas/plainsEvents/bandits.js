exports.event = 
{
    descriptionSolo : "You encounter a group of bandits that demand payment to let you pass. You don't have any money, so you quickly leave.",
    descriptionMulti : "You encounter a group of bandits that demand payment to let you pass. Nobody has any money, so you quickly leave.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 5,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Coin",
            consumed: true,
            involuntary: true,
            quantity: 30,
            descriptionSolo: "You encounter a group of bandits that demand payment to let you pass. You don't want any trouble, so you pay them and continue.",
            descriptionMulti: "You encounter a group of bandits that demand payment to let you pass. You don't want any trouble, so you pay them and continue."
        },
        {
            item: "Magic Wand",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You encounter a group of bandits that demand payment to let you pass. You swiftly blow them up, scattering bits and pieces across the lush green grass.",
            descriptionMulti: "You encounter a group of bandits that demand payment to let you pass. One of your party members swiftly blows them up, scattering bits and pieces across the lush green grass."
        },
        {
            item: "Shortsword",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You encounter a group of bandits that demand payment to let you pass. You take out your sword and beat them into submission.",
            descriptionMulti: "You encounter a group of bandits that demand payment to let you pass. One of your party members grabs a sword and beats them single-handedly."
        },
        {
            item: "Dwarven Sword",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You encounter a group of bandits that demand payment to let you pass. You take out your sword and beat them into submission.",
            descriptionMulti: "You encounter a group of bandits that demand payment to let you pass. One of your party members grabs a sword and beats them single-handedly."
        },
        {
            item: "Identity Disk",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You encounter a group of bandits that demand payment to let you pass. You take out your disk swiftly take care of them.",
            descriptionMulti: "You encounter a group of bandits that demand payment to let you pass. One of your party members grabs a disk and beats them single-handedly."
        },
        {
            item: "Elven Sword",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You encounter a group of bandits that demand payment to let you pass. You take out your sword and beat them into submission.",
            descriptionMulti: "You encounter a group of bandits that demand payment to let you pass. One of your party members grabs a sword and beats them single-handedly."
        },
        {
            item: "Spellbook",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You encounter a group of bandits that demand payment to let you pass. You cast a spell on them that makes them vomit frogs, slipping past in the confusion.",
            descriptionMulti: "You encounter a group of bandits that demand payment to let you pass. One of your party members casts a spell that makes them vomit frogs and the party slips past them in the confusion."
        },
        {
            item: "Fairy",
            consumed: false,
            quantity: 1,
            descriptionSolo: "You encounter a group of bandits that demand payment to let you pass. Your fairy companion casts a spell that makes them laugh uncontrollably, allowing you to sneak past them.",
            descriptionMulti: "You encounter a group of bandits that demand payment to let you pass. One of your party members' fairy companion casts a spell that makes them laugh uncontrollably, allowing you to sneak past them."
        }
    ],
    toArea: null
}