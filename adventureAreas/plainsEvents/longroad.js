exports.event = 
{
    descriptionSolo : "You walk across a long road. There are patches of flowers beside it giving the area many vibrant colors.",
    descriptionMulti : "You walk across a long road. There are patches of flowers beside it giving the area many vibrant colors.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 5,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "none",
            consumed: false,
            quantity: 1,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    ],
    toArea: null
}