exports.event = 
{
    descriptionSolo : "You come across a large lake. The sun reflects off the water surface.",
    descriptionMulti : "You come across a large lake. The sun reflects off the water surface.",

    loot:
    {
        name: "Fish",
        each: false,
        minQuantity: 1,
        maxQuantity: 10,
        conditional: true,
        conditionalRequirements:
        {
            items: ["Fishing Rod"],
            consumed: false,
            quantity: 1,
            descriptionSolo: "You encounter a large lake. You decide to try your luck and start fishing for a while.",
            descriptionMulti: "You encounter a large lake. One of your party members decides to try fishing for a while."
        }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "none",
            consumed: false,
            quantity: 1,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    ],
    toArea: null
}