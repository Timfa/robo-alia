exports.event = 
{
    descriptionSolo : "You see a cart with a broken wheel on the road. The owner standing next to it seems to be busy trying to fix it and doesn't notice you.",
    descriptionMulti : "You see a cart with a broken wheel on the road. The owner standing next to it seems to be busy trying to fix it and doesn't notice your party.",

    loot:
    {
            name: "Apple",
            each: false,
            minQuantity: 50,
            maxQuantity: 100,
            conditional: true,
            conditionalRequirements:
            {
                items: ["Coin"],
                consumed: true,
                quantity: 200,
                descriptionSolo: "You see a cart with a broken wheel on the road. The owner says he can fix the wheel, but he won't be in time to sell his wares. He offers you a good bargain to take them off his hands.",
                descriptionMulti: "You see a cart with a broken wheel on the road. The owner says he can fix the wheel, but he won't be in time to sell his wares. He offers you a good bargain to take them off his hands, which one of your party members gladly accepts."
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "none",
            consumed: false,
            quantity: 1,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    ],
    toArea: null
}