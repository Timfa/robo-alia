exports.event = 
{
    descriptionSolo : "You arrive at the beach. You collect some seashells.",
    descriptionMulti : "You arrive at the beach. You collect some seashells with your party.",

    loot:
    {
        name: "Seashell",
        each: true,
        minQuantity: 6,
        maxQuantity: 30,
        conditional: false,
        conditionalRequirements:
        {
            items: ["none"],
            consumed: true,
            quantity: 5,
            descriptionSolo: "",
            descriptionMulti: ""
        }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [ ],
    toArea: 
    { 
        areaName: "Ocean",
        conditional: true,
        conditionalRequirements:
        {
            items: ["Ship"],
            consumed: false,
            quantity: 1,
            descriptionSolo: "You arrive at the beach. You collect some seashells. You decide to use your ship and sail off onto the ocean.",
            descriptionMulti: "You arrive at the beach. You collect some seashells with your party. One of your party members mentions owning a ship, and suggests using it. You climb aboard and set off."
        }
    }
}