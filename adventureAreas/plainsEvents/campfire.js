exports.event = 
{
    descriptionSolo : "Walking along the road, you encounter a group of people eating food at a campfire. You join them and listen to their stories, also telling a few of your own.",
    descriptionMulti : "Walking along the road, you encounter a group of people eating food at a campfire. You join them and listen to their stories, also well as telling a few of your own.",

    loot:
    {
            name: "nothing",
            each: false,
            minQuantity: 1,
            maxQuantity: 1,
            conditional: false,
            conditionalRequirements:
            {
                items: ["none"],
                consumed: true,
                quantity: 5,
                descriptionSolo: "",
                descriptionMulti: ""
            }
    },

    killMember: false,
    forceEnd: false,
    exceptions: 
    [
        {
            item: "Tea Bag",
            consumed: true,
            quantity: 1,
            descriptionSolo: "Walking along the road, you encounter a group of people eating food at a campfire. You join them and listen to their stories, also telling a few of your own over a nice cup of tea.",
            descriptionMulti: "Walking along the road, you encounter a group of people eating food at a campfire. You join them and listen to their stories, also telling a few of your own. One of your party members brews a nice cup of tea."
        }
    ],
    toArea: null
}