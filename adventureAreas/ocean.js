exports.area = 
{
    name: "Ocean",
    canStart: false,
    events:[]
}

var fs = require("fs");

fs.readdir("./adventureAreas/oceanEvents/", function(err, files) {
    files.forEach(function(file) 
    {
        exports.area.events.push(require("./oceanEvents/" + file).event);
    });
})