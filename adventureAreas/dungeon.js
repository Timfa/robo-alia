exports.area =
{
    name: "Dungeon",
    canStart: true,
    events:[]
}

var fs = require("fs");

fs.readdir("./adventureAreas/dungeonEvents/", function (err, files) {
    files.forEach(function(file) 
    {
        exports.area.events.push(require("./dungeonEvents/" + file).event);
    });
})