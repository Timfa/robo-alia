exports.adventures = []

var fs = require("fs");

var read = function(err, files) {
    files.forEach(function(file) 
    {
        if(file.substr(file.length-2, 2) == "js")
            exports.adventures.push(require('./adventureAreas/' + file).area);
    });
}

fs.readdir("./adventureAreas/", read);